/**
 * @file Window.cpp
 * @author Tomas Polasek
 * @brief SDL2 window abstraction.
 */

#include "VisualizerWindow.h"

namespace Util
{
    VisualizerWindow::VisualizerWindow(std::size_t width, std::size_t height, const std::string &title)
    {
        init(width, height, title);
    }

    VisualizerWindow::~VisualizerWindow()
    {
        destroy();
    }

    void VisualizerWindow::init(std::size_t width, std::size_t height, const std::string &title)
    {
        destroy();

        mWindow = SDL_CreateWindow(title.c_str(),
                                   SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                   width, height,
                                   SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if (!mWindow)
        {
            throw std::runtime_error(std::string("Unable to create SDL window! : ") + SDL_GetError());
        }

        mContext = SDL_GL_CreateContext(mWindow);

        if (!mContext)
        {
            destroy();
            throw std::runtime_error(std::string("Unable to create OpenGL context for SDL window! : ") + SDL_GetError());
        }

        // VSync.
        SDL_GL_SetSwapInterval(1);

        // Pink clear color, used for visual error detection.
        //glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
        // Normal black clear color.
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // Create required OpenGL structures for rendering plane.
        mPlane.prepare();

        /*
         * Create basic GLSL shade program, where:
         *   a) Vertex shader is a simple pass-through shader.
         *   b) Fragment shader uses given texture and applies it to
         *        the rendering plane. X texturing coordinate is used
         *        normally and Y coordinate has to be flipped.
         */
        mProgram =  Util::GLSLProgram{
            {GL_VERTEX_SHADER, "#version 330 core\n"
                                   "layout(location = 0) in vec3 vertPos;\n"
                                   "layout(location = 1) in vec2 vertUv;\n"
                                   "out vec2 uv;"
                                   "void main() {\n"
                                   "  gl_Position = vec4(vertPos, 1.0);\n"
                                   "  uv = vertUv;\n"
                                   "}"},
            {GL_FRAGMENT_SHADER, "#version 330 core\n"
                                   "uniform sampler2D frameTexture;\n"
                                   "in vec2 uv;\n"
                                   "out vec3 color;\n"
                                   "void main() {\n"
                                   "  color = vec3(1.0, 0.0, 1.0);\n"
                                   "  color = texture(frameTexture, vec2(uv.x, 1.0f - uv.y)).rgb;\n"
                                   "}"}};
    }

    bool VisualizerWindow::pollEvents()
    {
        bool exit{false};

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_KEYUP:
                case SDL_KEYDOWN:
                { // Handle keyboard input.
                    mKeyboard.processEvent(event);
                    break;
                }
                case SDL_QUIT:
                { // Handle quit event.
                    exit = true;
                    break;
                }
                case SDL_WINDOWEVENT:
                { // Handle window events.
                    processWindowEvent(event);
                    break;
                }
                default:
                { // Unhandled event.
                    break;
                }
            }
        }

        return exit;
    }

    void VisualizerWindow::draw(guint textureId)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mProgram.use();

        glUniform1i(mProgram.getUniformLocation("frameTexture"), 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureId);

        mPlane.render();

        glBindTexture(GL_TEXTURE_2D, 0);
        glActiveTexture(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);

        SDL_GL_SwapWindow(mWindow);
    }

    void VisualizerWindow::destroy()
    {
        mPlane.destroy();

        if (mContext)
        {
            SDL_GL_DeleteContext(mContext);
            mContext = nullptr;
        }
        if (mWindow)
        {
            SDL_DestroyWindow(mWindow);
            mWindow = nullptr;
        }
    }

    void VisualizerWindow::getDimensions(int &width, int &height) const
    {
        SDL_GetWindowSize(mWindow, &width, &height);
    }

    Display *VisualizerWindow::getDisplay() const
    {
        return glXGetCurrentDisplay();
    }

    guintptr VisualizerWindow::createWorkerContext()
    {
        auto originalCtx = SDL_GL_GetCurrentContext();
        auto workerCtx = SDL_GL_CreateContext(mWindow);
        // Return to the original context.
        SDL_GL_MakeCurrent(mWindow, originalCtx);

        return reinterpret_cast<guintptr>(workerCtx);
    }

    void VisualizerWindow::processWindowEvent(const SDL_Event &event)
    {
        switch (event.window.event)
        {
            case SDL_WINDOWEVENT_RESIZED:
            { // Process resizing of the window.
                windowResized(event.window.data1, event.window.data2);
                break;
            }
            default:
            { // Do nothing for unknown event types.
                break;
            }
        }
    }

    void VisualizerWindow::windowResized(GLsizei width, GLsizei height)
    {
        glViewport(0, 0, width, height);

        if (mCbWindowResized)
        { // Trigger the callback.
            mCbWindowResized(width, height);
        }
    }
}
