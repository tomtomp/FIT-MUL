/**
 * @file Pipeline.cpp
 * @author Tomas Polasek
 * @brief GStreamer pipeline used in music visualization.
 */

#include <mutex>
#include <Types.h>
#include "Pipeline.h"

namespace Gst
{
    Pipeline::~Pipeline()
    {
        // Free bus.
        if (mBus)
        {
            gst_object_unref(mBus);
        }

        // Set the pipeline to free itself.
        if (mPipeline)
        {
            gst_object_unref(mPipeline);
        }

        /*
        // TODO - Cleanup destructor, segment into methods.
        // If we have a frame mapped, we should free it...
        if (mCurrentBuffer)
        {
            gst_video_frame_unmap(&mCurrentFrame);
            gst_buffer_unref(mCurrentBuffer);
        }

        // Free any pending buffers in each of the queues.
        while (g_async_queue_length(mBufferOutput) != 0)
        {
            // TODO - Change to reinterpret_cast, when we are sure no other objects end up in this queue?
            GstBuffer *buffer = GST_BUFFER(g_async_queue_pop(mBufferOutput));
            gst_buffer_unref(buffer);
        }
        while (g_async_queue_length(mBufferInput) != 0)
        {
            // TODO - Change to reinterpret_cast, when we are sure no other objects end up in this queue?
            GstBuffer *buffer = GST_BUFFER(g_async_queue_pop(mBufferInput));
            gst_buffer_unref(buffer);
        }

        // Free queues.
        g_async_queue_unref(mBufferOutput);
        g_async_queue_unref(mBufferInput);
         */
    }

    void Pipeline::initMute(Display *display, guintptr workerContext, const std::string &clientName, const std::string &source)
    {
        // Based on : https://gstreamer.freedesktop.org/documentation/tutorials/basic/dynamic-pipelines.html .

        // We have a mute pipeline.
        mMutePipeline = true;

        std::cout << "Using \"" << source << "\" as source for mute pipeline." << std::endl;

        // Create required elements :
        //   |Source| -> |Audio Convert| -> |Visualizer| -> |Caps Filter| -> |Fake Sink|
        mSource = gst_element_factory_make(source.c_str(), "source");
        g_object_set(mSource, "qos", true, nullptr);
        mVisualizerConvert = gst_element_factory_make("audioconvert", "visualizer-audio-convert");
        g_object_set(mVisualizerConvert, "qos", true, nullptr);
        mVisualizer = gst_element_factory_make("visualizer", "visualizer");
        g_object_set(mVisualizer, "qos", true, nullptr);
        mVisualizerResCapsFilter = gst_element_factory_make("capsfilter", "visualizer-caps-filter");
        g_object_set(mVisualizerResCapsFilter, "qos", true, nullptr);
        mVisualizerFakeSink = gst_element_factory_make("appsink", "visualizer-fake-sink");
        g_object_set(mVisualizerFakeSink, "qos", true, nullptr);

        g_object_set(mSource, "client-name", clientName.c_str(), nullptr);

        mPipeline = gst_pipeline_new("visualizer-mute-pipeline");

        if (!mVisualizer)
        {
            throw std::runtime_error("Gst:: Unable to load Visualizer plugin, please make sure it is correctly "
                                         "installed, or set the location of the plugin with "
                                         "GST_PLUGIN_PATH=/path/to/plugin/ , where gstvisualizer.so is located.");
        }

        if (!mSource || !mVisualizerConvert || !mVisualizer || !mVisualizerResCapsFilter || !mVisualizerFakeSink)
        {
            throw std::runtime_error("Gst:: Unable to create a part of the pipeline!");
        }

        // Synchronize on clock.
        /*
        //g_object_set(mVisualizerFakeSink, "sync", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "async", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "max-lateness", 1, nullptr);
        //g_object_set(mVisualizerFakeSink, "qos", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "sync", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "async", true, nullptr);
        g_object_set(mVisualizerFakeSink, "wait-on-eos", false, nullptr);
        g_object_set(mVisualizerFakeSink, "drop", true, nullptr);
        g_object_set(mVisualizerFakeSink, "max-buffers", 15, nullptr);
        // TODO - Maybe the pulsesrc bitrate is uncapped?
        // TODO - Are the buffers in appsink being freed?
        //g_object_set(mVisualizerFakeSink, "qos", true, nullptr);
        g_object_set(mVisualizerQueue, "max-size-buffers", 0, "max-size-bytes", 0, "max-size-time", 0, nullptr);
        g_object_set(mVisualizerQueue, "leaky", 2, nullptr);

        g_object_set(mAudioSink, "sync", true, nullptr);
        g_object_set(mAudioSink, "async-handling", true, nullptr);

        g_object_set(mSource, "provide-clock", false, nullptr);
        g_object_set(mSource, "do-timestamp", true, nullptr);
        g_object_set(mSource, "slave-method", 1, nullptr);
        g_object_set(mSource, "buffer-time", 20000, nullptr);
        g_object_set(mSource, "latency-time", 10000, nullptr);
         */

        g_object_set(mTeeAudioConvert, "caps",
                     gst_caps_new_simple("audio/x-raw",
                                         "format", G_TYPE_STRING, "S16",
                                         "rate", G_TYPE_INT, 44100,
                                         "channels", G_TYPE_INT, 1,
                                         nullptr),
                     nullptr);

        // Set resolution capabilities filter.
        g_object_set(mVisualizerResCapsFilter, "caps",
                     gst_caps_new_simple(VIDEO_TYPE_STR,
                                         "format", G_TYPE_STRING, VIDEO_FORMAT_STR,
                                         "width", G_TYPE_INT, VIDEO_WIDTH,
                                         "height", G_TYPE_INT, VIDEO_HEIGHT,
                                         nullptr),
                     nullptr);
        gst_video_info_set_format(&mFrameInfo, VIDEO_FORMAT, VIDEO_WIDTH, VIDEO_HEIGHT);

        // Add all of the elements into created pipeline.
        gst_bin_add_many(GST_BIN(mPipeline),
                         mSource,
                         mVisualizerConvert, mVisualizer, mVisualizerResCapsFilter,
                         mVisualizerFakeSink, nullptr);

        // Link main branch
        if (!gst_element_link_many(mVisualizerConvert, mVisualizer,
                                   mVisualizerResCapsFilter, mVisualizerFakeSink, nullptr))
        {
            throw std::runtime_error("Gst:: Unable to link elements in the visualizer branch!");
        }

        /*
         * Other connections (mSource -> mTeeAudioConvert)
         * need to be made after pads are generated!
         */

        initializeFinish(display, workerContext);
    }

    void Pipeline::init(Display *display, guintptr workerContext, const std::string &clientName, const std::string &source)
    {
        // Based on : https://gstreamer.freedesktop.org/documentation/tutorials/basic/dynamic-pipelines.html .

        // We have a reproduction pipeline.
        mMutePipeline = false;

        std::cout << "Using \"" << source << "\" as source for pipeline." << std::endl;

        // Create required elements :
        //   |Source| -> |Audio Convert| -> |Tee| -> |Queue| -> |Audio Convert| -> |Visualizer| -> |Caps Filter| -> |Fake Sink|
        //                                  |   | -> |Queue| -> |Audio Convert| -> |Auto Audio Sink|
        mSource = gst_element_factory_make(source.c_str(), "source");
        g_object_set(mSource, "qos", true, nullptr);
        mTeeAudioConvert = gst_element_factory_make("audioconvert", "tee-audio-convert");
        g_object_set(mTeeAudioConvert, "qos", true, nullptr);
        mTee = gst_element_factory_make("tee", "tee");
        g_object_set(mTee, "qos", true, nullptr);
        mVisualizerQueue = gst_element_factory_make("queue", "visualizer-queue");
        g_object_set(mVisualizerQueue, "qos", true, nullptr);
        mVisualizerConvert = gst_element_factory_make("audioconvert", "visualizer-audio-convert");
        g_object_set(mVisualizerConvert, "qos", true, nullptr);
        mVisualizer = gst_element_factory_make("visualizer", "visualizer");
        g_object_set(mVisualizer, "qos", true, nullptr);
        mVisualizerResCapsFilter = gst_element_factory_make("capsfilter", "visualizer-caps-filter");
        g_object_set(mVisualizerResCapsFilter, "qos", true, nullptr);
        mVisualizerFakeSink = gst_element_factory_make("appsink", "visualizer-fake-sink");
        g_object_set(mVisualizerFakeSink, "qos", true, nullptr);
        mAudioQueue = gst_element_factory_make("queue", "audio-queue");
        g_object_set(mAudioQueue, "qos", true, nullptr);
        mAudioConvert = gst_element_factory_make("audioconvert", "audio-audio-convert");
        g_object_set(mAudioConvert, "qos", true, nullptr);
        mAudioSink = gst_element_factory_make("autoaudiosink", "audio-sink");
        g_object_set(mAudioSink, "qos", true, nullptr);

        g_object_set(mSource, "client-name", clientName.c_str(), nullptr);

        mPipeline = gst_pipeline_new("visualizer-pipeline");

        if (!mVisualizer)
        {
            throw std::runtime_error("Gst:: Unable to load Visualizer plugin, please make sure it is correctly "
                                         "installed, or set the location of the plugin with "
                                         "GST_PLUGIN_PATH=/path/to/plugin/ , where gstvisualizer.so is located.");
        }

        if (!mSource || !mTeeAudioConvert || !mTee ||
            !mVisualizerQueue || !mVisualizerConvert || !mVisualizer || !mVisualizerResCapsFilter ||
            !mVisualizerFakeSink || !mAudioQueue || !mAudioConvert || !mAudioSink)
        {
            throw std::runtime_error("Gst:: Unable to create a part of the pipeline!");
        }

        // Synchronize on clock.
        /*
        //g_object_set(mVisualizerFakeSink, "sync", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "async", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "max-lateness", 1, nullptr);
        //g_object_set(mVisualizerFakeSink, "qos", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "sync", true, nullptr);
        //g_object_set(mVisualizerFakeSink, "async", true, nullptr);
        g_object_set(mVisualizerFakeSink, "wait-on-eos", false, nullptr);
        g_object_set(mVisualizerFakeSink, "drop", true, nullptr);
        g_object_set(mVisualizerFakeSink, "max-buffers", 15, nullptr);
        // TODO - Maybe the pulsesrc bitrate is uncapped?
        // TODO - Are the buffers in appsink being freed?
        //g_object_set(mVisualizerFakeSink, "qos", true, nullptr);
        g_object_set(mVisualizerQueue, "max-size-buffers", 0, "max-size-bytes", 0, "max-size-time", 0, nullptr);
        g_object_set(mVisualizerQueue, "leaky", 2, nullptr);

        g_object_set(mAudioSink, "sync", true, nullptr);
        g_object_set(mAudioSink, "async-handling", true, nullptr);

        g_object_set(mSource, "provide-clock", false, nullptr);
        g_object_set(mSource, "do-timestamp", true, nullptr);
        g_object_set(mSource, "slave-method", 1, nullptr);
        g_object_set(mSource, "buffer-time", 20000, nullptr);
        g_object_set(mSource, "latency-time", 10000, nullptr);
         */

        g_object_set(mTeeAudioConvert, "caps",
                     gst_caps_new_simple("audio/x-raw",
                                         "format", G_TYPE_STRING, "S16",
                                         "rate", G_TYPE_INT, 44100,
                                         "channels", G_TYPE_INT, 1,
                                         nullptr),
                     nullptr);

        // Set resolution capabilities filter.
        g_object_set(mVisualizerResCapsFilter, "caps",
                     gst_caps_new_simple(VIDEO_TYPE_STR,
                                         "format", G_TYPE_STRING, VIDEO_FORMAT_STR,
                                         "width", G_TYPE_INT, VIDEO_WIDTH,
                                         "height", G_TYPE_INT, VIDEO_HEIGHT,
                                         nullptr),
                     nullptr);
        gst_video_info_set_format(&mFrameInfo, VIDEO_FORMAT, VIDEO_WIDTH, VIDEO_HEIGHT);

        // Add all of the elements into created pipeline.
        gst_bin_add_many(GST_BIN(mPipeline),
                         mSource, mTeeAudioConvert, mTee,
                         mVisualizerQueue, mVisualizerConvert, mVisualizer, mVisualizerResCapsFilter,
                         mAudioQueue, mAudioConvert, mAudioSink,
                         mVisualizerFakeSink, nullptr);

        // Link visualizer branch.
        if (!gst_element_link_many(mVisualizerQueue, mVisualizerConvert, mVisualizer,
                                   mVisualizerResCapsFilter, mVisualizerFakeSink, nullptr))
        {
            throw std::runtime_error("Gst:: Unable to link elements in the visualizer branch!");
        }
        // Link audio branch.
        if (!gst_element_link_many(mAudioQueue, mAudioConvert, mAudioSink, nullptr))
        {
            throw std::runtime_error("Gst:: Unable to link elements in the audio branch!");
        }

        // Connect tee to branches.
        GstPad *visualizerQueuePad = gst_element_get_static_pad(mVisualizerQueue, "sink");
        GstPad *teeVisualizerPad = gst_element_get_request_pad(mTee, "src_%u");
        GstPad *teeAudioPad = gst_element_get_request_pad(mTee, "src_%u");
        if (gst_pad_link(teeVisualizerPad, visualizerQueuePad) != GST_PAD_LINK_OK)
        {
            throw std::runtime_error("Gst:: Unable to link tee to Visualizer branch!");
        }

        GstPad *audioQueuePad = gst_element_get_static_pad(mAudioQueue, "sink");
        if (gst_pad_link(teeAudioPad, audioQueuePad) != GST_PAD_LINK_OK)
        {
            throw std::runtime_error("Gst:: Unable to link tee to audio sink branch!");
        }

        // Connect converted audio from source to tee.
        if (!gst_element_link(mTeeAudioConvert, mTee))
        {
            throw std::runtime_error("Gst:: Unable to link converted source audio to tee!");
        }

        /*
         * Other connections (mSource -> mTeeAudioConvert)
         * need to be made after pads are generated!
         */

        initializeFinish(display, workerContext);
    }

    void Pipeline::initializeFinish(Display *display, guintptr workerContext)
    {
        // Setup and connect signal for getting the video output from fake sink.
        //g_object_set(G_OBJECT(mVisualizerFakeSink), "signal-handoffs", true, NULL);
        //g_signal_connect(mVisualizerFakeSink, "handoff", G_CALLBACK(&Pipeline::videoHandoff), this);
        g_object_set (mVisualizerFakeSink, "emit-signals", TRUE, NULL);
        g_signal_connect (mVisualizerFakeSink, "new-sample", G_CALLBACK(&Pipeline::appVideoHandoff), this);

        // Bus used for communication using messages.
        mBus = gst_element_get_bus(mPipeline);

        // Prepare context handle used in the pipeline.
        GstGLDisplay *gstDisplay = GST_GL_DISPLAY(gst_gl_display_x11_new_with_display(display));
        GstGLContext *gstWorkerContext = gst_gl_context_new_wrapped(gstDisplay, workerContext,
                                                                    GST_GL_PLATFORM_GLX, GST_GL_API_OPENGL3);
        if (!gstWorkerContext)
        {
            throw std::runtime_error("Gst:: Unable to create GStreamer OpenGL worker context handle!");
        }

        // Set the display context to elements which require it.
        GstContext *displayContext = gst_context_new(GST_GL_DISPLAY_CONTEXT_TYPE, true);
        gst_context_set_gl_display(displayContext, gstDisplay);
        gst_element_set_context(GST_ELEMENT(mVisualizer), displayContext);

        // Set the OpenGL context to elements which require it.
        GstContext *appContext = gst_context_new("gst.gl.app_context", true);
        GstStructure *appContextStruct = gst_context_writable_structure(appContext);
        gst_structure_set(appContextStruct, "context", GST_TYPE_GL_CONTEXT, gstWorkerContext, nullptr);
        gst_element_set_context(GST_ELEMENT(mVisualizer), appContext);

        // Create queues used for passing buffers from and to pipeline threads.
        mBufferInput = g_async_queue_new();
        mBufferOutput = g_async_queue_new();
        if (!mBufferInput || !mBufferOutput)
        {
            throw std::runtime_error("Gst:: Unable to create asynchronous queues!");
        }
    }

    void Pipeline::setupUri(const std::string &uri)
    {
        // Test input.
        g_object_set(mSource, "uri", uri.c_str(), nullptr);

        // We cannot connect to source pads immediately, since they don't exist yet...
        //mPadAddedConnection = g_signal_connect(mSource, "pad-added", G_CALLBACK(&Pipeline::sourcePadAdded), this);
        mNoMorePadsConnection = g_signal_connect(mSource, "no-more-pads", G_CALLBACK(&Pipeline::sourceNoMorePads), this);
    }

    void Pipeline::setupMicrophone()
    {
        if (mMutePipeline)
        { // Mute pipeline -> connect source to visualizer convert.
            if (!gst_element_link(mSource, mVisualizerConvert))
            {
                throw std::runtime_error("Gst:: Unable to connect audio source to visualizer audio convert!");
            }
        }
        else
        { // Mute pipeline -> connect source to tee convert.
            if (!gst_element_link(mSource, mTeeAudioConvert))
            {
                throw std::runtime_error("Gst:: Unable to connect audio source to tee audio convert!");
            }
        }
    }

    void Pipeline::play()
    {
        if (gst_element_set_state(mPipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE)
        {
            throw std::runtime_error("Gst:: Unable to change the state of the pipeline to PLAYING!");
        }
    }

    void Pipeline::pause()
    {
        if (gst_element_set_state(mPipeline, GST_STATE_PAUSED) == GST_STATE_CHANGE_FAILURE)
        {
            throw std::runtime_error("Gst:: Unable to change the state of the pipeline to PLAYING!");
        }
    }

    void Pipeline::stop()
    {
        if (gst_element_set_state(mPipeline, GST_STATE_READY) == GST_STATE_CHANGE_FAILURE)
        {
            throw std::runtime_error("Gst:: Unable to change the state of the pipeline to PLAYING!");
        }
    }

    void Pipeline::pollEvents()
    {
        GstMessage *msg{nullptr};
        //while ((msg = gst_bus_pop_filtered(mBus, MSG_FILTER)) != nullptr)
        while ((msg = gst_bus_pop_filtered(mBus, GST_MESSAGE_ANY)) != nullptr)
        {
            switch (GST_MESSAGE_TYPE(msg))
            {
                case GST_MESSAGE_ERROR:
                {
                    printError(msg);
                    break;
                }

                case GST_MESSAGE_STATE_CHANGED:
                {
                    printStateChange(msg);
                    break;
                }

                case GST_MESSAGE_EOS:
                {
                    std::cout << "Gst:: End of stream reached!" << std::endl;
                    break;
                }

                case GST_MESSAGE_NEED_CONTEXT:
                {
                    const gchar *ctxType{nullptr};
                    gst_message_parse_context_type(msg, &ctxType);
                    std::cout << "Gst:: Requesting context! : " << ctxType << std::endl;
                    break;
                }

                case GST_MESSAGE_LATENCY:
                {
                    std::cout << "Gst:: Recalculating latency from " << GST_MESSAGE_SRC_NAME(msg) << std::endl;
                    gst_bin_recalculate_latency(GST_BIN(mPipeline));
                    std::cout << "Gst:: New latency is "
                              << gst_pipeline_get_latency(GST_PIPELINE(mPipeline)) / 1000000000.0 << "s" << std::endl;
                    break;
                }

                case GST_MESSAGE_NEW_CLOCK:
                {
                    GstClock *clock;
                    gst_message_parse_new_clock(msg, &clock);
                    std::cout << "Gst:: New clock \"" << (clock ? GST_OBJECT_NAME(clock) : "NULL") << "\"" << std::endl;
                    break;
                }

                case GST_MESSAGE_QOS:
                {
                    GstFormat format;
                    guint64 processed;
                    guint64 dropped;

                    gst_message_parse_qos_stats(msg, &format, &processed, &dropped);
                    std::cout << "Gst:: QOS : format " << gst_format_get_name(format) << " processed " << processed
                              << " dropped " << dropped << " from " << GST_MESSAGE_SRC_NAME(msg) << std::endl;

                    break;
                }

                // TODO - Add messages!

                default:
                {
                    std::cout << "Unknown message, type: " << gst_message_type_get_name(GST_MESSAGE_TYPE(msg)) << std::endl;
                    break;
                }
            }
        }
    }

    /*
    void Pipeline::sourcePadAdded(GstElement *element, GstPad *pad, Pipeline *pp)
    {
        // Get the sink pad for the convert element.
        GstPad *audioSinkPad{gst_element_get_static_pad(pp->mAudioConvert, "sink")};
        if (gst_pad_is_linked(audioSinkPad))
        { // It's already linked, no need to continue.
            gst_object_unref(audioSinkPad);
            return;
        }

        // Check the type and capabilities of received pad.
        GstCaps *padCaps{gst_pad_query_caps(pad, nullptr)};
        GstStructure *padStruct{gst_caps_get_structure(padCaps, 0u)};
        const gchar *padType{gst_structure_get_name(padStruct)};
        if (!g_str_has_prefix(padType, AUDIO_FORMAT))
        { // Not interested in received pad type...
            gst_caps_unref(padCaps);
            gst_object_unref(audioSinkPad);
            return;
        }

        // Attempt to link it.
        if (GST_PAD_LINK_FAILED(gst_pad_link(pad, audioSinkPad)))
        { // Failed to link.
            g_printerr("Gst:: Failed to link to source pad with type %s!\n", padType);
        }
        else
        { // Link successful, we can disconnect the signal.
            g_signal_handler_disconnect(pp->mSource, pp->mPadAddedConnection);
        }

        gst_caps_unref(padCaps);
        gst_object_unref(audioSinkPad);
    }
     */

    void Pipeline::printError(GstMessage *msg)
    {
        GError *err{nullptr};
        gchar *debugInfo{nullptr};
        gst_message_parse_error(msg, &err, &debugInfo);

        std::cerr << "Gst:: Received error from element " << GST_OBJECT_NAME(msg->src) << " : " << err->message
                  << "\n\t Debug information: " << (debugInfo ? debugInfo : "None!") << std::endl;
    }

    void Pipeline::printStateChange(GstMessage *msg)
    {
        if (GST_MESSAGE_SRC(msg) == GST_OBJECT(mPipeline))
        {
            GstState oldState;
            GstState newState;
            GstState pendingState;
            gst_message_parse_state_changed (msg, &oldState, &newState, &pendingState);
            std::cout << "Gst:: Pipeline changed state from " << gst_element_state_get_name(oldState)
                      << " to " << gst_element_state_get_name(newState)
                      << " pending " << gst_element_state_get_name(pendingState) << std::endl;
        }
    }

    void Pipeline::sourceNoMorePads(GstElement *element, Pipeline *pp)
    {
        /*
        // Link audio stream to converter.
        if (!gst_element_link(pp->mSource, pp->mAudioConvert))
        {
            g_printerr("Gst:: Unable to connect source audio stream, perhaps there is none...\n");
        }

        // Link video stream to converter.
        if (!gst_element_link(pp->mSource, pp->mVideoConvertRgb))
        {
            g_printerr("Gst:: Unable to connect source video stream, perhaps there is none...\n");
        }
         */

        // Link audio.
        if (pp->mMutePipeline)
        { // Mute pipeline -> link straight to Visualizer convert.
            if (!gst_element_link(pp->mSource, pp->mVisualizerConvert))
            {
                g_printerr("Gst:: Unable to connect source to Visualizer audio converter!\n");
            }
        }
        else
        { // Reproduction pipeline -> link to tee convert.
            if (!gst_element_link(pp->mSource, pp->mTeeAudioConvert))
            {
                g_printerr("Gst:: Unable to connect source to tee audio converter!\n");
            }

        }

        g_signal_handler_disconnect(pp->mSource, pp->mNoMorePadsConnection);
    }

    void Pipeline::videoHandoff(GstElement *sink, GstBuffer *buffer, GstPad *pad, Pipeline *pp)
    {
        // TODO - ?!?
        if (g_async_queue_length(pp->mBufferInput) > 4)
        {
            while (g_async_queue_length(pp->mBufferInput) > 0)
            {
                GstBuffer *buff = GST_BUFFER(g_async_queue_pop(pp->mBufferInput));
                gst_buffer_unref(buff);
            }
        }
        // Pass the new buffer to the window rendering thread.
        gst_buffer_ref(buffer);
        g_async_queue_push(pp->mBufferInput, buffer);

        /*
        std::cout << "Got new buffer: PTS " << GST_TIME_AS_MSECONDS(GST_BUFFER_PTS(buffer))
                  << " DTS " << GST_TIME_AS_MSECONDS(GST_BUFFER_DTS(buffer)) << std::endl;
                  */

        // Free one used buffer, if there are any.
        if (g_async_queue_length(pp->mBufferOutput) > 1)
        {
            // TODO - Change to reinterpret_cast, when we are sure no other objects end up in this queue?
            GstBuffer *usedBuffer = GST_BUFFER(g_async_queue_pop(pp->mBufferOutput));
            gst_buffer_unref(usedBuffer);
        }
    }

    void Pipeline::setupFrameInfo(gint width, gint height)
    {
        // Set resolution capabilities filter.
        g_object_set(mVisualizerResCapsFilter, "caps",
                     gst_caps_new_simple(VIDEO_TYPE_STR,
                                         "format", G_TYPE_STRING, VIDEO_FORMAT_STR,
                                         "width", G_TYPE_INT, width,
                                         "height", G_TYPE_INT, height,
                                         nullptr),
                     nullptr);
    }

    GLuint Pipeline::mapFrameTexture()
    {
        // Inspired by : https://github.com/GStreamer/gst-plugins-base/blob/master/tests/examples/gl/sdl/sdlshare.c

        //std::cout << "Length of input queue " << g_async_queue_length(mBufferInput) << std::endl;

        // If there is a new frame ready, switch to it, else use the old one.
        if (g_async_queue_length(mBufferInput) > 1)
        {
            if (mCurrentBuffer)
            { // If there was a frame already mapped, free it.
                gst_video_frame_unmap(&mCurrentFrame);
                g_async_queue_push(mBufferOutput, mCurrentBuffer);
            }

            // Get the next buffer.
            // TODO - Change to reinterpret_cast, when we are sure no other objects end up in this queue?
            mCurrentBuffer = GST_BUFFER(g_async_queue_pop(mBufferInput));
            GstVideoMeta *meta = gst_buffer_get_video_meta(mCurrentBuffer);
            gst_video_info_set_format(&mFrameInfo, VIDEO_FORMAT, meta->width, meta->height);
            // Map the frame.
            if (!gst_video_frame_map(&mCurrentFrame, &mFrameInfo, mCurrentBuffer, FRAME_MAP_FLAGS))
            {
                throw std::runtime_error("Gst:: Unable to map frame!");
            }
        }

        if (mCurrentBuffer)
        { // If there is a frame mapped -> use it.
            return *reinterpret_cast<guint*>(mCurrentFrame.data[0]);
        }
        else
        { // Else return texture ID 0.
            return 0;
        }
    }

    void Pipeline::setUserShaders(const std::string &vertSrc, const std::string &fragSrc)
    {
        if (!vertSrc.empty())
        {
            g_object_set(mVisualizer, "vertex-source", vertSrc.c_str(), nullptr);
        }
        if (!fragSrc.empty())
        {
            g_object_set(mVisualizer, "fragment-source", fragSrc.c_str(), nullptr);
        }
    }

    void Pipeline::quit()
    {
        gst_element_set_state(mPipeline, GST_STATE_PAUSED);

        /*
        gst_element_set_state(mPipeline, GST_STATE_PAUSED);
        pollEvents();

        gst_element_set_state(mPipeline, GST_STATE_READY);
        pollEvents();
         */

        /*
        gst_element_send_event(mPipeline, gst_event_new_eos());
        GstMessage *msg = gst_bus_timed_pop_filtered(GST_ELEMENT_BUS(mPipeline),
                                                     GST_CLOCK_TIME_NONE,
                                                     static_cast<GstMessageType>(GST_MESSAGE_EOS | GST_MESSAGE_ERROR));
        if (!msg)
        {
            std::cerr << "Unable to gracefully stop the pipeline, forcing quit..." << std::endl;
        }
         */

        gst_element_set_state(mPipeline, GST_STATE_NULL);
        pollEvents();

    }

    void Pipeline::appVideoHandoff(GstElement *sink, Pipeline *pp)
    {
        // TODO - ?!?
        if (g_async_queue_length(pp->mBufferInput) > 2)
        {
            while (g_async_queue_length(pp->mBufferInput) > 0)
            {
                GstBuffer *buff = GST_BUFFER(g_async_queue_pop(pp->mBufferInput));
                gst_buffer_unref(buff);
            }
        }

        // Get the buffer.
        GstSample *sample;
        g_signal_emit_by_name(sink, "pull-sample", &sample);

        if (!sample)
        { // No sample found.
            g_print("No sample!\n");
            return;
        }

        GstBuffer *buffer{gst_sample_get_buffer(sample)};

        if (!buffer)
        { // No buffer found.
            g_print("No buffer!\n");
            return;
        }

        // Pass the new buffer to the window rendering thread.
        //gst_buffer_ref(buffer);
        g_async_queue_push(pp->mBufferInput, buffer);
        // TODO - No more buffer drops? Awesome!!
        //gst_buffer_unref(buffer);

        /*
        std::cout << "Got new buffer: PTS " << GST_TIME_AS_MSECONDS(GST_BUFFER_PTS(buffer))
                  << " DTS " << GST_TIME_AS_MSECONDS(GST_BUFFER_DTS(buffer)) << std::endl;
                  */

        // Free one used buffer, if there are any.
        if (g_async_queue_length(pp->mBufferOutput) > 1)
        {
            // TODO - Change to reinterpret_cast, when we are sure no other objects end up in this queue?
            GstBuffer *usedBuffer = GST_BUFFER(g_async_queue_pop(pp->mBufferOutput));
            gst_buffer_unref(usedBuffer);
        }
    }

    void Pipeline::nextFreqSampling()
    {
        gint current{1};
        g_object_get(mVisualizer, "freq-sampling", &current, NULL);
        gint newVal{current};
        if (++newVal >= VISUALIZER_SAMPLING_TYPE_LAST)
        {
            newVal = 1;
        }

        std::cout << "Setting freq-sampling from " << freqSamplingToString(current)
                  << " to " << freqSamplingToString(newVal) << std::endl;

        g_object_set(mVisualizer, "freq-sampling", newVal, NULL);
    }

    void Pipeline::nextFreqEdge()
    {
        gint current{1};
        g_object_get(mVisualizer, "freq-edge", &current, NULL);
        gint newVal{current};
        if (++newVal >= VISUALIZER_EDGE_TYPE_LAST)
        {
            newVal = 1;
        }

        std::cout << "Setting freq-edge from " << freqEdgeSamplingToString(current)
                  << " to " << freqEdgeSamplingToString(newVal) << std::endl;

        g_object_set(mVisualizer, "freq-edge", newVal, NULL);
    }

    void Pipeline::changeFftHistory(float change)
    {
        unsigned int current{VISUALIZER_MIN_HISTORY};
        g_object_get(mVisualizer, "fft-history", &current, NULL);
        unsigned int newVal{current};
        if (newVal * change < VISUALIZER_MIN_HISTORY)
        {
            newVal = VISUALIZER_MIN_HISTORY;
        }
        else if (newVal * change > VISUALIZER_MAX_HISTORY)
        {
            newVal = VISUALIZER_MAX_HISTORY;
        }
        else
        {
            newVal *= change;
        }

        std::cout << "Setting fft-history from " << current << " to " << newVal << std::endl;

        g_object_set(mVisualizer, "fft-history", newVal, NULL);
    }

    void Pipeline::changeFftBins(float change)
    {
        unsigned int current{VISUALIZER_MIN_BINS};
        g_object_get(mVisualizer, "fft-bins", &current, NULL);
        unsigned int newVal{std::max(current, 1u)};
        if (newVal * change < VISUALIZER_MIN_BINS)
        {
            newVal = VISUALIZER_MIN_BINS;
        }
        else if (newVal * change > VISUALIZER_MAX_BINS)
        {
            newVal = VISUALIZER_MAX_BINS;
        }
        else
        {
            newVal *= change;
        }

        std::cout << "Setting fft-bins from " << current << " to " << newVal << std::endl;

        g_object_set(mVisualizer, "fft-bins", newVal, NULL);
    }

    void Pipeline::setAutoFftBins()
    {
        unsigned int newVal{0};
        std::cout << "Setting fft-bins to automatic." << std::endl;

        g_object_set(mVisualizer, "fft-bins", newVal, NULL);
    }

    void Pipeline::flipFftNormalize()
    {
        gboolean current{false};
        g_object_get(mVisualizer, "fft-normalize", &current, NULL);
        gboolean newVal{!current};

        std::cout << "Setting fft-normalize from " << current << " to " << newVal << std::endl;

        g_object_set(mVisualizer, "fft-normalize", newVal, NULL);
    }

    void Pipeline::flipFftLogBins()
    {
        gboolean current{false};
        g_object_get(mVisualizer, "fft-log-bins", &current, NULL);
        gboolean newVal{!current};

        std::cout << "Setting fft-log-bins from " << current << " to " << newVal << std::endl;

        g_object_set(mVisualizer, "fft-log-bins", newVal, NULL);
    }

    void Pipeline::changeFftNumSamples(float change)
    {
        unsigned int current{VISUALIZER_MIN_NUM_SAMPLES};
        g_object_get(mVisualizer, "fft-num-samples", &current, NULL);
        unsigned int newVal{current};
        if (newVal * change < VISUALIZER_MIN_NUM_SAMPLES)
        {
            newVal = VISUALIZER_MIN_NUM_SAMPLES;
        }
        else if (newVal * change > VISUALIZER_MAX_NUM_SAMPLES)
        {
            newVal = VISUALIZER_MAX_NUM_SAMPLES;
        }
        else
        {
            newVal *= change;
        }

        std::cout << "Setting fft-num-samples from " << current << " to " << newVal << std::endl;

        g_object_set(mVisualizer, "fft-num-samples", newVal, NULL);
    }

    void Pipeline::nextFftWindow()
    {
        gint current{1};
        g_object_get(mVisualizer, "fft-window", &current, NULL);
        gint newVal{current};
        if (++newVal >= VISUALIZER_WINDOW_TYPE_LAST)
        {
            newVal = 1;
        }

        std::cout << "Setting fft-windows from " << windowToString(current)
                  << " to " << windowToString(newVal) << std::endl;

        g_object_set(mVisualizer, "fft-window", newVal, NULL);
    }

    const char *Pipeline::freqSamplingToString(gint value)
    {
        // TODO - Ugly, but working for now.
        switch (value)
        {
            case VISUALIZER_SAMPLING_TYPE_NEAREST:
            {
                return "Nearest";
            }
            case VISUALIZER_SAMPLING_TYPE_LINEAR:
            {
                return "Linear";
            }
            default:
            {
                return "Unknown";
            }
        }
    }

    const char *Pipeline::freqEdgeSamplingToString(gint value)
    {
        // TODO - Ugly, but working for now.
        switch (value)
        {
            case VISUALIZER_EDGE_TYPE_REPEAT:
            {
                return "Repeat";
            }
            case VISUALIZER_EDGE_TYPE_MIRRORED_REPEAT:
            {
                return "RepeatMirrored";
            }
            case VISUALIZER_EDGE_TYPE_CLAMP_TO_EDGE:
            {
                return "ClampToEdge";
            }
            case VISUALIZER_EDGE_TYPE_CLAMP_TO_BORDER:
            {
                return "ClampToBorder";
            }
            default:
            {
                return "Unknown";
            }
        }
    }

    const char *Pipeline::windowToString(gint value)
    {
        // TODO - Ugly, but working for now.
        switch (value)
        {
            case VISUALIZER_WINDOW_TYPE_RECTANGULAR:
            {
                return "Rectangular";
            }
            case VISUALIZER_WINDOW_TYPE_HAMMING:
            {
                return "Hamming";
            }
            case VISUALIZER_WINDOW_TYPE_HANN:
            {
                return "Hanning";
            }
            case VISUALIZER_WINDOW_TYPE_BARTLETT:
            {
                return "Bartlett";
            }
            case VISUALIZER_WINDOW_TYPE_BLACKMAN:
            {
                return "Blackman";
            }
            default:
            {
                return "Unknown";
            }
        }
    }

    void Pipeline::changeFftHistoryCoeff(float change)
    {
        gfloat current{VISUALIZER_MIN_NUM_SAMPLES};
        g_object_get(mVisualizer, "fft-history-coeff", &current, NULL);
        // Clamp to <MIN, MAX>
        gfloat newVal{std::min(VISUALIZER_MAX_HISTORY_COEFF, std::max(VISUALIZER_MIN_HISTORY_COEFF, current + change))};

        std::cout << "Setting fft-history-coeff from " << current << " to " << newVal << std::endl;

        g_object_set(mVisualizer, "fft-history-coeff", newVal, NULL);
    }
}
