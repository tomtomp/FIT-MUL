#version 330 core

#define M_PI 3.141592653589793238462643383
#define M_2PI 2.0 * 3.141592653589793238462643383

uniform sampler2D freqs;
uniform float width;
uniform float height;
uniform float time;

in vec2 uv;
out vec4 color;

void main() {
    /*
    float yCoord = (pow(2.0, 8.0 * (1.0 - uv.y)) - 1.0) / 255.0;
    float xCoord = 1.0 - uv.x;
    float mag = texture(freqs, vec2(xCoord, yCoord)).r;
    color = vec4(abs(sin(time) * log(mag + 1.0)), abs(cos(time) * log2(mag + 1.0)), abs(sin(time + M_PI/3.0) * log(mag + 1.0) / log(10)), 1.0);
    */
    /*
    const vec2 middle = vec2(0.5, 0.5);
    vec2 mUv = uv - middle;
    float normalLength = length(middle);

    float r = length(mUv) / normalLength;
    float phi = (atan(mUv.y, mUv.x) / (M_2PI)) + 0.5;

    float xCoord = (pow(2.0, 8.0 * phi) - 1.0) / 255.0;
    float yCoord = r;

    float mag = texture(freqs, vec2(xCoord, yCoord)).r;
    color = vec4(abs(sin(time) * log(mag + 1.0)), abs(cos(time) * log2(mag + 1.0)), abs(sin(time + M_PI/3.0) * log(mag + 1.0) / log(10)), 1.0);
    */

    float smoothArea = 0.05;
    float yCoord = 1.0 - uv.y;
    float xCoord = (pow(2.0, 8.0 * uv.x) - 1.0) / 255.0;
    float mag = log2(1.0 + texture(freqs, vec2(xCoord, 0.0)).r);
    smoothArea = abs(texture(freqs, vec2(xCoord, 0.01)).r - mag);
    mag = 10 * log(pow(2.0, 0.5 * mag)) / log(10.0);
    if (yCoord < mag + smoothArea)
    {
        float pst = 1.0 - smoothstep(0.0, 0.05, yCoord - mag);
        float rx = (0.2 - uv.x) * 2.0;
        float r = pst * exp(-M_PI * rx * rx);
        float gx = (0.5 - uv.x) * 2.0;
        float g = pst * exp(-M_PI * gx * gx);
        float bx = (0.8 - uv.x) * 2.0;
        float b = pst * exp(-M_PI * bx * bx);

        color = vec4(r, g, b, 1.0);
    }
    else
    {
        color = vec4(0.0, 0.0, 0.0, 1.0);
    }
}

