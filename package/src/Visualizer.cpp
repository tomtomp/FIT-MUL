/**
 * @file Visualizer.cpp
 * @author Tomas Polasek
 * @brief Contains the main function for the Visualizer application.
 */

#include "Visualizer.h"

int main(int argc, char *argv[])
{
    try {
        VisualizerApp app(argc, argv);
        return app.run();
    } catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
    } catch (...) {
        std::cerr << "Unknown exception escaped the main application!" << std::endl;
    }

    return EXIT_FAILURE;
}
