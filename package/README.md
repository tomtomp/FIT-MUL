Vizualizace hudby
=================
Autor: Tomáš Polášek (xpolas34)

Tento projekt byl vytvořen jako součást hodnocení z předmětu Multimedia (MUL) na Fakultě Informačních Technologií v Brně.

Jeho cílem bylo vytvořit aplikace, která bude v reálném čase vizualizovat hudbu.

Obsah archivu
=============

Archiv obsahuje následující soubory a složky: 
 * README.md - Popis archivu a překladu aplikace.
 * README.pdf - Dokumentace.
 * CMakeLists.txt - CMake překladový skript.
 * src/ a include/ - Zdrojové soubory okenní aplikace.
 * gst/ - Zdrojové soubory modulu pro GStreamer.
 * bin/ - Výstupní adresář pro přeloženou aplikace, obsahuje příklady vizualizačních programů.

Překlad
=======

Aplikace se skládá ze dvou částí - modul do frameworku GStreamer a okenní uživatelské rozhranní. Pro jejich překlad jsou nutné následující závislosti: 
 * Překladač jazyka C++ s podporou standardu C++14 (tesrováno na GCC 7.3.0).
 * CMake alespoň verze 3.6 (testováno na CMake 3.10.2).
 * Knihovnu SDL2.
 * Knihovnu OpenGL.
 * Knihovnu GStreamer a její moduly GL, audio, video a FFT (testováno na GStreamer 1.12.4).
 * Operační systém Linux používající X11 (nutné kvůli sdílení OpenGL kontextu).

Pro její překlad je nutné provést následující kroky: 
```
cd repo_root
mkdir build && cd build
cmake ..
make -j4
```

Po dokončení překladu lze nalézt přeloženou aplikaci **Visualizer** a modul pro GStreamer **gstvisualizer.so** ve složce **bin**.

Použití
=======

Po překladu a přepnutí do složky **bin** lze aplikaci spustit např. následovně: 
```
GST_PLUGIN_PATH=`pwd` ./Visualizer -m -F ./Circle.frag -q
```

Pro podrobnější popis jednotlivých funkcí lze použít přepínač **-h**, nebo příslušnou sekci v dokumentaci.

