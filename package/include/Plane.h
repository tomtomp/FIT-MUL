/**
 * @file Plane.h
 * @author Tomas Polasek
 * @brief Simple render-able plane for OpenGL.
 */

#ifndef MUL_VISUALIZER_PLANE_H
#define MUL_VISUALIZER_PLANE_H

#include "Types.h"

/// Namespace for utility functions and classes.
namespace Util
{
    /// Renderable plane.
    class Plane
    {
    public:
        /// Construct the Triangle and all the buffers.
        Plane();

        /// Prepare buffers.
        void prepare();

        /// Render the triangle
        void render();

        /// Destroy the buffers.
        ~Plane();

        /// Destroy all the GL buffers.
        void destroy();
    private:
        /// 2 triangles -> 6 vertices.
        static constexpr std::size_t NUM_VERTICES{6};
        static constexpr std::size_t VALS_PER_VERTEX{3};
        static constexpr std::size_t VALS_PER_UV{2u};

        /// Vertex data for the Plane.
        static const GLfloat VERTEX_BUFFER_DATA[NUM_VERTICES * VALS_PER_VERTEX];

        /// UV data for the Plane.
        static const GLfloat UV_BUFFER_DATA[NUM_VERTICES * VALS_PER_UV];

        /// The vertex array ID.
        GLuint mVAId{0u};
        /// The vertex buffer ID.
        GLuint mVBId{0u};
        /// The uv buffer ID.
        GLuint mUvBId{0u};
    protected:
    }; // class Plane
} // namespace Util

#endif //MUL_VISUALIZER_PLANE_H
