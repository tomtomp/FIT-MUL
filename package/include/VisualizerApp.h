/**
 * @file VisualizerApp.h
 * @author Tomas Polasek
 * @brief Main application class for the Visualizer app.
 */

#ifndef MUL_VISUALIZER_VISUALIZERAPP_H
#define MUL_VISUALIZER_VISUALIZERAPP_H

#include "Types.h"
#include "Util.h"
#include "VisualizerWindow.h"
#include "Pipeline.h"

class VisualizerApp : public Util::NonCopyMove
{
public:
    /// Initialize the application.
    VisualizerApp(int argc, char *argv[]);
    /// Free resources and end application.
    ~VisualizerApp();

    /// Run the application, returns code, which should be returned by the whole program.
    int run();
private:
    /// Starting width of the window.
    static constexpr std::size_t WINDOW_WIDTH{1024u};
    /// Starting height of the window.
    static constexpr std::size_t WINDOW_HEIGHT{768u};
    /// Title of the window
    static constexpr const char *WINDOW_TITLE{"Visualizer"};
    /// Formatting string for parsing arguments using getopt.
    static constexpr const char *GETOPT_FORMAT{":f:mV:F:q"};
    /// Help message for the application.
    // TODO - Add application usage and keyboard shortcuts.
    static constexpr const char *HELP_MESSAGE{"Usage: Visualizer {-f URI | -m} [-V VERTEX_SRC_FILE] [-F FRAGMENT_SRC_FILE] [-h]\n"
                                                  "\t-f: Specification of which file to play.\n"
                                                  "\t-m: Use system audio input as input to visualize.\n"
                                                  "\tWARNING: Precisely one of -f or -m MUST be specified!\n"
                                                  "\t-V: File containing vertex shader source code.\n"
                                                  "\t-F: File containing fragment shader source code.\n"
                                                  "\t-q: Mute output sound.\n"
                                                  "\t-h: Print this message.\n"
                                                  "Application control scheme: \n"
                                                  "\tEscape: Close the application.\n"
                                                  "\tSpace: Pause/play the input stream.\n"
                                                  "\tr: Reload the vertex and fragment source files.\n"
                                                  "\ts: Next frequency map sampling mode.\n"
                                                  "\te: Next frequency map edge sampling mode.\n"
                                                  "\tw: Next windowing function type.\n"
                                                  "\ty and h: Increase/decrease history smoothing coefficient.\n"
                                                  "\tu and j: Increase/decrease history depth.\n"
                                                  "\ti and k: Increase/decrease number of frequency bins.\n"
                                                  "\to and l: Increase/decrease size of FFT buffer.\n"
                                                  "\tm: Enable/disable automatic number of bins.\n"
                                                  "\tn: Enable/disable normalization of frequency bins.\n"
                                                  "\tb: Enable/disable logarithmic frequency bin distribution.\n"
                                                  "\n"};

    /// By how much should the history size change on button press.
    static constexpr float VISUALIZER_HISTORY_CHANGE{2.0f};
    /// By how much should the bins number change on button press.
    static constexpr float VISUALIZER_BINS_CHANGE{2.0f};
    /// By how much should the number of samples change on button press.
    static constexpr float VISUALIZER_SAMPLES_CHANGE{2.0f};
    /// By how much should the history coefficient change on button press.
    static constexpr float VISUALIZER_HISTORY_COEFF_CHANGE{0.05f};

    /// Initialize SDL library.
    void initSDL();

    /// Initialize GLEW library.
    void initGLEW();

    /// Initializer the GStreamer library.
    void initGst(int argc, char *argv[]);

    /// Parse command line arguments.
    void parseArgs(int argc, char *argv[]);

    /// Print help message.
    void printHelp();

    /**
     * If given string contains filename, converts the filename into URI.
     * @param audioInput String containing audio input.
     */
    void convertInputToUri(std::string &audioInput);

    /// Reload user specified shader files.
    void reloadUserShaders();

    /// Set user specified shader source codes.
    void useUserShaders();

    /// Main display window.
    Util::VisualizerWindow mWindow;
    /// GStreamer pipeline used for audio processing.
    Gst::Pipeline mPipeline;

    /// URI specifying which audio source should the application use.
    std::string mAudioInput;
    /// When set, the application will use microphone instead of mAudioInput.
    bool mUseMicrophone;
    /// Should there be sound reproduction, or should the application be mute.
    bool mMute;
    /// Filename of the vertex shader used in visualizer plugin.
    std::string mVertSrcFilename;
    /// Vertex shader source code used in visualizer plugin.
    std::string mVertSrc;
    /// Filename of the fragment shader used in visualizer plugin.
    std::string mFragSrcFilename;
    /// Fragment shader source code used in visualizer plugin.
    std::string mFragSrc;
protected:
}; // class VisualizerApp

#endif //MUL_VISUALIZER_VISUALIZERAPP_H
