/**
 * @file Keyboard.h
 * @author Tomas Polasek
 * @brief Keyboard handling class
 */

#ifndef MUL_VISUALIZER_KEYBOARD_H
#define MUL_VISUALIZER_KEYBOARD_H

#include "Types.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Keyboard handling class.
    class Keyboard
    {
    public:
        /// Type of the action function, called when corresponding key is pressed.
        using ActionFun = std::function<void()>;

        /// Default constructor.
        Keyboard() = default;

        /**
         * Set action for given key combination.
         * @param type Type, e.g. SDL_KEYUP or SDL_KEYDOWN.
         * @param mod Modifiers, e.g. KMOD_NONE. KMOD_NUM and KMOD_CAPS are disabled.
         * @param key Key to react to, e.g. SDLK_A.
         * @param fun Function to call
         */
        void setAction(uint32_t type, uint16_t mod, SDL_Keycode key, ActionFun fun)
        { mMapping[{key, removeNumCapsLock(mod), type}] = fun; }

        /**
         * Reset action for given key combination.
         * @param type Type, e.g. SDL_KEYUP or SDL_KEYDOWN.
         * @param mod Modifiers, e.g. KMOD_NONE. KMOD_NUM and KMOD_CAPS are disabled.
         * @param key Key to react to, e.g. SDLK_A.
         * @param fun Function to call
         */
        void setAction(uint32_t type, uint16_t mod, SDL_Keycode key)
        { mMapping.erase({key, removeNumCapsLock(mod), type}); }

        /**
         * Process given event and call corresponding callbacks.
         * @param event Event to process.
         */
        void processEvent(const SDL_Event &event)
        {
            decltype(mMapping.begin()) findIt = mMapping.find({event.key.keysym.sym, removeNumCapsLock(event.key.keysym.mod), event.type});
            if (findIt != mMapping.end())
            {
                findIt->second();
            }
        }
    private:
        /// Helper structure for searching in map.
        struct KeyCombination
        {
            KeyCombination(SDL_Keycode keyV, uint16_t modV, uint32_t typeV) :
                key{keyV}, mod{modV}, type{typeV} { }

            /// Keycode.
            SDL_Keycode key;
            /// Modifiers.
            uint16_t mod;
            /// Action - e.g. SDL_KEYDOWN.
            uint32_t type;

            /// Comparison operator.
            bool operator<(const KeyCombination &rhs) const
            { return (key < rhs.key) || ((key == rhs.key) && (mod < rhs.mod)) || ((key == rhs.key) && (mod == rhs.mod) && (type < rhs.type)); }
            /// Comparison equal operator.
            bool operator==(const KeyCombination &rhs) const
            { return (key == rhs.key) && (mod == rhs.mod) && (type == rhs.type); }
        };

        /// Remove capslock and numlock flags from given mod.
        uint16_t removeNumCapsLock(uint16_t mod)
        { return mod & ~(KMOD_CAPS | KMOD_NUM); }

        /// Mapping from keys to actions.
        std::map<KeyCombination, ActionFun> mMapping;
    protected:
    }; // class Keyboard
} // namespace Util

#endif //MUL_VISUALIZER_KEYBOARD_H
