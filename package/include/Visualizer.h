/**
 * @file Visualizer.h
 * @author Tomas Polasek
 * @brief Contains the main function for the Visualizer application.
 */

#ifndef MUL_VISUALIZER_VISUALIZER_H
#define MUL_VISUALIZER_VISUALIZER_H

#include "VisualizerApp.h"

#endif //MUL_VISUALIZER_VISUALIZER_H
