/**
 * @file GstPipeline.h
 * @author Tomas Polasek
 * @brief GStreamer pipeline used in music visualization.
 */

#ifndef MUL_VISUALIZER_PIPELINE_H
#define MUL_VISUALIZER_PIPELINE_H

#include "Types.h"
#include "Util.h"
#include "Timer.h"

#include "gstvisualizer.h"

/// Namespace containing classes and types used in communication with GStreamer library.
namespace Gst
{
    /// GStreamer pipeline abstraction.
    class Pipeline : public Util::NonCopyMove
    {
    public:
        /// Testing input URL.
        static constexpr const char *TEST_INPUT1{"https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm"};
        static constexpr const char *TEST_INPUT2{"http://us3.internet-radio.com:8007/listen.pls&t=.m3u"};
        static constexpr const char *TEST_INPUT3{"file:////home/tomtomp/HddHome/tomtomp/School/FIT-MIT1/MUL/proj/bin/test.avi"};
        static constexpr const char *TEST_INPUT4{"file:///home/tomtomp/HddHome/tomtomp/Music/old/Alice _ Pogo-pAwR6w2TgxY.m4a"};
        /// Source for processing URI input.
        static constexpr const char *URI_SOURCE{"uridecodebin"};
        /// Source for processing microphone input.
        //static constexpr const char *MICROPHONE_SOURCE{"autoaudiosrc"};
        static constexpr const char *MICROPHONE_SOURCE{"pulsesrc"};

        /// Create empty pipeline.
        Pipeline() = default;
        /// Destroy the pipeline.
        ~Pipeline();

        /**
         * Initialize the pipeline in mute mode.
         * @param display Display used for the window.
         * @param workerContext OpenGL context which will be used within the pipeline.
         * @param clientName Name of the client, used for pulse audio client.
         * @param source Source element used in the pipeline.
         */
        void initMute(Display *display, guintptr workerContext, const std::string &clientName, const std::string &source);

        /**
         * Initialize the pipeline with sound reproduction.
         * @param display Display used for the window.
         * @param workerContext OpenGL context which will be used within the pipeline.
         * @param clientName Name of the client, used for pulse audio client.
         * @param source Source element used in the pipeline.
         */
        void init(Display *display, guintptr workerContext, const std::string &clientName, const std::string &source);

        /**
         * Setup playback with given URI.
         * @param uri File identifier.
         */
        void setupUri(const std::string &uri);

        /**
         * Setup playback from microphone.
         */
        void setupMicrophone();

        /// Resume playing.
        void play();

        /// Pause playback.
        void pause();

        /// Stop playback.
        void stop();

        /// Poll events from the bus and process them.
        void pollEvents();

        /**
         * Set requested information for frame mapping.
         * @param width Requested width of the frame.
         * @param height Requested height of the frame.
         */
        void setupFrameInfo(gint width, gint height);

        /**
         * Map next frame texture from the input queue.
         * If there is a new frame in the input queue, this method also
         * pushes the last frame buffer into output queue for freeing.
         * @return Returns ID of the texture or 0 if there are none.
         */
        GLuint mapFrameTexture();

        /**
         * Set user provided shaders. Shaders are used only if they are not empty!
         * @param vertSrc Vertex shader source code.
         * @param fragSrc Fragment shader source code.
         */
        void setUserShaders(const std::string &vertSrc, const std::string &fragSrc);

        /// Quit pipeline.
        void quit();

        /// Switch Visualizer to the next frequency sampling type.
        void nextFreqSampling();
        /// Convert frequency sampling type to string.
        const char* freqSamplingToString(gint value);
        /// Switch Visualizer to the next frequency edge sampling type.
        void nextFreqEdge();
        /// Convert frequency edge sampling type to string.
        const char* freqEdgeSamplingToString(gint value);
        /// Change Visualizer FFT history by given value.
        void changeFftHistory(float change);
        /// Change Visualizer FFT number of bins by given value.
        void changeFftBins(float change);
        /// Set number of FFT bins to automatic.
        void setAutoFftBins();
        /// Switch Visualizer FFT normalization on or off.
        void flipFftNormalize();
        /// Switch Visualizer FFT logarithmic frequency bins.
        void flipFftLogBins();
        /// Change Visualizer FFT number of samples by given value.
        void changeFftNumSamples(float change);
        /// Switch Visualizer to the next window applied before FFT.
        void nextFftWindow();
        /// Convert FFT windo type to string.
        const char* windowToString(gint value);
        /// Change Visualizer FFT history coefficient.
        void changeFftHistoryCoeff(float change);
    private:
        /// Filter used for filtering the bus messages.
        static constexpr GstMessageType MSG_FILTER{static_cast<GstMessageType>(GST_MESSAGE_STATE_CHANGED
                                                                               | GST_MESSAGE_ERROR
                                                                               | GST_MESSAGE_EOS
                                                                               | GST_MESSAGE_NEED_CONTEXT)};
        /// Required video type from the source.
        static constexpr const char *VIDEO_TYPE_STR{"video/x-raw"};
        /// Required video format from the source.
        static constexpr const char *VIDEO_FORMAT_STR{"RGBA"};
        /// Default width of the video generated by Visualizer.
        static constexpr gint VIDEO_WIDTH{1024};
        /// Default height of the video generated by Visualizer.
        static constexpr gint VIDEO_HEIGHT{768};
        /// Reqired video format from the source.
        static constexpr GstVideoFormat VIDEO_FORMAT{GST_VIDEO_FORMAT_RGBA};
        /// Flags used for mapping the frames.
        static constexpr GstMapFlags FRAME_MAP_FLAGS{static_cast<GstMapFlags>(GST_MAP_READ | GST_MAP_GL)};

        /// Callback when new pad is added to the source.
        static void sourcePadAdded(GstElement *element, GstPad *pad, Pipeline *pp);
        /// Callback when no more pads will be created by the source.
        static void sourceNoMorePads(GstElement *element, Pipeline *pp);

        /// Video handoff from the fake sink.
        static void videoHandoff(GstElement *sink, GstBuffer *buffer, GstPad *pad, Pipeline *pp);

        /// Video handoff from the app sink.
        static void appVideoHandoff(GstElement *sink, Pipeline *pp);

        /// Print error message.
        void printError(GstMessage *msg);

        /// Print state change message.
        void printStateChange(GstMessage *msg);

        /// Finishing initialization steps common to both mute and non-mute pipeline.
        void initializeFinish(Display *display, guintptr workerContext);

        /* Pipeline layout:
         *   |Source| -> |Audio Convert| -> |Tee| -> |Queue| -> |Audio Convert| -> |Visualizer| -> |Caps Filter| -> |Fake Sink|
         *                                  |   | -> |Queue| -> |Audio Convert| -> |Auto Audio Sink|
         */
        /* Mute pipeline layout:
         *   |Source| -> |Audio Convert| -> |Visualizer| -> |Caps Filter| -> |Fake Sink|
         */
        /// Is the current pipeline the mute one?
        bool mMutePipeline{false};
        /// Handle for the whole pipeline.
        GstElement *mPipeline{nullptr};
        /// Decoder/demuxer.
        GstElement *mSource{nullptr};
        /// Converting audio from source.
        GstElement *mTeeAudioConvert{nullptr};
        /// Split audio stream into 2.
        GstElement *mTee{nullptr};
        /// Queue for the top branch.
        GstElement *mVisualizerQueue{nullptr};
        //GstElement *mVisualizerQueue2{nullptr};
        /// Audio conversion for top branch.
        GstElement *mVisualizerConvert{nullptr};
        /// Visualizer plugin.
        GstElement *mVisualizer{nullptr};
        /// Caps filter used to set output resolution of the Visualizer plugin.
        GstElement *mVisualizerResCapsFilter{nullptr};
        /// Fake sink for visualizer output.
        GstElement *mVisualizerFakeSink{nullptr};
        /// Queue for the bottom branch.
        GstElement *mAudioQueue{nullptr};
        /// Audio conversion for bottom branch.
        GstElement *mAudioConvert{nullptr};
        /// Sink for playing the audio.
        GstElement *mAudioSink{nullptr};
        /// Message bus.
        GstBus *mBus{nullptr};
        /// Connection identifier for the "pad-added" signal.
        gulong mNoMorePadsConnection{0};

        /// Queue used for asynchronously storing buffers from the fake sink.
        GAsyncQueue *mBufferInput{nullptr};
        /// Queue used for asynchronously storing used buffers which can be freed.
        GAsyncQueue *mBufferOutput{nullptr};
        /// Currently used buffer.
        GstBuffer *mCurrentBuffer{nullptr};
        /// Current buffer mapped frame.
        GstVideoFrame mCurrentFrame;
        /// Video information used for mapping frames.
        GstVideoInfo mFrameInfo;
    protected:
    }; // class Pipeline
} // namespace Gst

#endif //MUL_VISUALIZER_PIPELINE_H
