/**
 * @file Types.h
 * @author Tomas Polasek
 * @brief Common includes and types.
 */

#ifndef MUL_VISUALIZER_TYPES_H
#define MUL_VISUALIZER_TYPES_H

// Standard library includes.
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include <exception>
#include <cstdint>
#include <functional>
#include <map>
#include <unordered_map>
#include <mutex>
#include <fstream>

// Headers specific to Linux.
#include <unistd.h>

// GStreamer includes.
#include <gst/gst.h>

// Includes required for the window and drawing.
//#include <GL/glew.h>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

// GStreamer OpenGL includes, need to be below GLEW...
#define GST_USE_UNSTABLE_API
#include <gst/gl/gl.h>
#include <gst/gl/x11/gstgldisplay_x11.h>
#include <GL/glx.h>
#undef GST_USE_UNSTABLE_API

#endif //MUL_VISUALIZER_TYPES_H
