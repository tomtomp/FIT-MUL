/**
 * @file Window.h
 * @author Tomas Polasek
 * @brief SDL2 window abstraction.
 */

#ifndef MUL_VISUALIZER_VISUALIZERWINDOW_H
#define MUL_VISUALIZER_VISUALIZERWINDOW_H

#include "Types.h"
#include "Util.h"
#include "Keyboard.h"
#include "GLSLProgram.h"
#include "Plane.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// SDL2 window abstraction.
    class VisualizerWindow : public Util::NonCopyMove
    {
    public:
        /// Type of callback triggered when window changes size.
        using WindowResizedFun = std::function<void(GLsizei width, GLsizei height)>;

        /// Initialize empty window.
        VisualizerWindow() = default;

        /**
         * Create and initialize a new window with given parameters.
         * @param width Width of the window.
         * @param height Height of the window.
         * @param title Title of the window.
         * @warning Destroys any old window!
         * @throws std::runtime_error When error occurs.
         */
        VisualizerWindow(std::size_t width, std::size_t height, const std::string &title);

        /**
         * Create and initialize a new window with given parameters.
         * @param width Width of the window.
         * @param height Height of the window.
         * @param title Title of the window.
         * @warning Destroys any old window!
         * @throws std::runtime_error When error occurs.
         */
        void init(std::size_t width, std::size_t height, const std::string &title);

        /// Free resources.
        virtual ~VisualizerWindow();

        /**
         * Poll events from SDL.
         * @return Returns true, if the application requested exit.
         */
        bool pollEvents();

        /**
         * Set action for given key combination.
         * @param type Type, e.g. SDL_KEYUP or SDL_KEYDOWN.
         * @param mod Modifiers, e.g. KMOD_NONE.
         * @param key Key to react to, e.g. SDLK_A.
         * @param fun Function to call
         */
        void setAction(uint32_t type, uint16_t mod, SDL_Keycode key, Util::Keyboard::ActionFun fun)
        { mKeyboard.setAction(type, mod, key, fun); }

        /**
         * Draw a frame.
         * @param textureId ID of the OpenGL texture containing frame data.
         */
        void draw(guint textureId);

        /**
         * Get dimensions of this window.
         * @param width Output for width.
         * @param height Output for height.
         */
        void getDimensions(int &width, int &height) const;

        /// Get display used by this window.
        Display *getDisplay() const;

        /// Create a new worker OpenGL context slaved to the window context.
        guintptr createWorkerContext();

        /// Set callback triggered when window changes size.
        void setWindowResizedCb(WindowResizedFun fun)
        { mCbWindowResized = fun; }

        /// Hide this window
        void hide()
        { SDL_HideWindow(mWindow); }
    private:
        /// Destroy all created objects.
        void destroy();

        /**
         * Process given window specific event.
         * @param event Event to process.
         */
        void processWindowEvent(const SDL_Event &event);

        /**
         * Process resizing of window to given size.
         * @param width New width of the window.
         * @param height New height of the window.
         */
        void windowResized(GLsizei width, GLsizei height);

        /// Keyboard handler.
        Util::Keyboard mKeyboard;
        /// Shader program used for drawing the rendering plane.
        Util::GLSLProgram mProgram;
        /// Rendering plane.
        Util::Plane mPlane;
        /// Window handle
        SDL_Window *mWindow{nullptr};
        /// OpenGL context for the window.
        SDL_GLContext mContext{nullptr};

        // Callback functions.
        /// Callback triggered when window changes size.
        WindowResizedFun mCbWindowResized{nullptr};
    protected:
    }; // class VisualizerWindow
} // namespace Util

#endif //MUL_VISUALIZER_VISUALIZERWINDOW_H
