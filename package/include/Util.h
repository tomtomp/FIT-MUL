/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Utility functions and classes.
 */

#ifndef MUL_VISUALIZER_UTIL_H
#define MUL_VISUALIZER_UTIL_H

#include "Types.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base class for non-copyable, non-movable classes.
    class NonCopyMove
    {
    public:
    private:
    protected:
        NonCopyMove() = default;
        virtual ~NonCopyMove() = default;

        NonCopyMove(const NonCopyMove &other) = delete;
        NonCopyMove(NonCopyMove &&other) = delete;
        NonCopyMove &operator=(const NonCopyMove &other) = delete;
        NonCopyMove &operator=(NonCopyMove &&other) = delete;
    }; // class NonCopyMove

    /**
     * Load file specified by filename into provided output string.
     * Original content of output string will be deleted!
     * @param filename Name of the file.
     * @param output File contents will be assigned to this string.
     * @throws std::runtime_error When error occurs.
     */
    void readFileIntoString(const std::string &filename, std::string &output);
} // namespace Util

#endif //MUL_VISUALIZER_UTIL_H
