/* GStreamer
 * Copyright (C) 2018 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstvisualizer
 *
 * The visualizer element creates audio visualization.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v audiotestsrc ! visualizer ! glimagesink
 * ]|
 * This pipeline visualizes testing sound.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/gstmemory.h>
#include "gstvisualizer.h"

// Memory manipulation functions.
#include "string.h"
#include "stdio.h"

GST_DEBUG_CATEGORY_STATIC (gst_visualizer_debug_category);
#define GST_CAT_DEFAULT gst_visualizer_debug_category

/* prototypes */

// Visualizer
/// Initialize elements in the freq sub-structure.
static void gst_visualizer_init_freq_texture(GstVisualizer *visualizer);
/// Prepare shader, may trigger re-compilation.
static GstGLShader *gst_visualizer_recompile_shader(GstVisualizer *visualizer);
/// Set user requested uniforms.
static gboolean gst_visualizer_set_custom_uniforms(GstVisualizer *visualizer);
/// Callback used when setting requested uniforms.
static gboolean gst_visualizer_set_uniform_callback(GQuark fieldId, const GValue *value,
    gpointer object);
/// Used in negotiation of video capabilities with other elements.
static gboolean gst_visualizer_propose_allocation_video(GstVisualizer *visualizer,
    gboolean needPool, GstCaps *caps, GstQuery *decideQuery, GstQuery *query);
/// Used in negotiation of audio capabilities with other elements.
static gboolean gst_visualizer_propose_allocation_audio(GstVisualizer *visualizer,
    gboolean needPool, GstCaps *caps, GstQuery *decideQuery, GstQuery *query);
/// Used in negotiation of video capabilities with other elements.
static gboolean gst_visualizer_decide_allocation_video(GstVisualizer *visualizer,
    GstCaps *caps, GstQuery *query);
/// Used in negotiation of audio capabilities with other elements.
static gboolean gst_visualizer_decide_allocation_audio(GstVisualizer *visualizer,
    GstCaps *caps, GstQuery *query);
/**
 * Main audio processing function.
 * @param visualizer Visualizer instance.
 * @param inBuf Input buffer containing audio data.
 * @return Returns TRUE, if everything went OK.
 */
static gboolean gst_visualizer_process_audio(GstVisualizer *visualizer, GstBuffer *inBuf);
/// Specialized audio processing for signed 16-bit integer samples.
static gboolean gst_visualizer_process_audio_S16(GstVisualizer *visualizer, void *data, gsize numSamples);
/// Specialized buffer creation for signed 16-bit integer samples..
static gboolean gst_visualizer_freq_data_S16(GstVisualizer *visualizer, gsize numFreqs, gsize numSamples);
/// Setup texture, may trigger upload of new data and texture creation.
static gboolean gst_visualizer_check_create_freq_texture(GstVisualizer *visualizer);
/// Send updated data from CPU to GPU.
static gboolean gst_visualizer_update_freq_texture(GstVisualizer *visualizer);
/**
 * Set required information based on audio format (S16, etc.) .
 * @param visualizer Visualizer instance.
 * @param audioInfo Information about audio received.
 * @return Returns TRUE if everything went OK.
 */
static gboolean gst_visualizer_set_freq_texture_info(GstVisualizer *visualizer, GstAudioInfo *audioInfo);
/// If required, re-creates buffers used by specialized audio processing functions.
static gboolean gst_visualizer_check_create_freq_data(GstVisualizer *visualizer);
/// If required, re-creates buffer used for storing frequency texture data.
static gboolean gst_visualizer_check_create_freq_buffer(GstVisualizer *visualizer);
/// Calculate number of frequencies based on number of samples.
static gsize gst_visualizer_calc_num_freqs(GstVisualizer *visualizer, gsize numSamples);
/// Helper function used for setting shader type.
static void gst_visualizer_set_shader_type(GstVisualizer *visualizer, GstVisualizerShaderType type);
/// Helper function used for setting texture sampling type.
static void gst_visualizer_set_sampling(GstVisualizer *visualizer, GstVisualizerSamplingType type);
/// Helper function used for setting FFT window type;
static void gst_visualizer_set_window(GstVisualizer *visualizer, GstVisualizerWindowType type);
/// Helper function used for setting texture edge sampling type.
static void gst_visualizer_set_edge(GstVisualizer *visualizer, GstVisualizerEdgeType type);
/// Shift frequency texture by one row in order to make space for new data.
static void gst_visualizer_shift_freq_buffer(GstVisualizer *visualizer);

// GstGLBaseFilter
static gboolean gst_visualizer_gl_start(GstGLBaseFilter *base);
static void gst_visualizer_gl_stop(GstGLBaseFilter *base);
//static gboolean gst_visualizer_gl_set_caps(GstGLBaseFilter *bt, GstCaps *inCaps,
//   GstCaps *outCaps)

// GstGLFilter
static gboolean gst_visualizer_filter(GstGLFilter *filter,
    GstBuffer *inBuf, GstBuffer *outBuf);
static gboolean gst_visualizer_filter_texture(GstGLFilter *filter,
    guint inTex, GstGLMemory *outTex);
static gboolean gst_visualizer_init_fbo(GstGLFilter *filter);
static GstCaps *gst_visualizer_transform_internal_caps(GstGLFilter *filter,
    GstPadDirection direction, GstCaps *caps, GstCaps *filterCaps);
static gboolean gst_visualizer_draw_callback(GstGLFilter *filter,
    guint inTex, gpointer stuff);

// GstBaseTransform
static GstCaps *gst_visualizer_transform_caps(GstBaseTransform *bt,
    GstPadDirection direction, GstCaps *caps, GstCaps *filterCaps);
static gboolean gst_visualizer_set_caps(GstBaseTransform *bt,
    GstCaps *inCaps, GstCaps *outCaps);
static gboolean gst_visualizer_get_unit_size(GstBaseTransform *trans,
    GstCaps *caps, gsize *size);
static gboolean gst_visualizer_propose_allocation(GstBaseTransform *trans,
    GstQuery *decideQuery, GstQuery *query);
static gboolean gst_visualizer_decide_allocation(GstBaseTransform *trans,
    GstQuery *query);
static GstCaps *gst_visualizer_fixate_caps(GstBaseTransform *trans,
    GstPadDirection direction, GstCaps *caps, GstCaps *otherCaps);

// GObject
static void gst_visualizer_set_property(GObject *object,
    guint propId, const GValue *value, GParamSpec *pSpec);
static void gst_visualizer_get_property(GObject *object,
    guint propId, GValue *value, GParamSpec *pSpec);
static void gst_visualizer_dispose(GObject *object);
static void gst_visualizer_finalize(GObject *object);

static gboolean plugin_init(GstPlugin *plugin);

/**
 * Get next power of two.
 * Credit:  https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
 * @param num Number to process.
 * @return Returns next power of two or 0, if the input is zero.
 */
static guint next_power_of_two(guint num);

enum
{
    PROP_0,

    // Shader controls.
    PROP_SHADER_TYPE,
    PROP_VERTEX_SRC,
    PROP_FRAGMENT_SRC,
    PROP_UNIFORMS,
    PROP_FREQ_SAMPLING,
    PROP_FREQ_EDGE,

    // FFT controls.
    PROP_FFT_HISTORY,
    PROP_FFT_BINS,
    PROP_FFT_NORMALIZE,
    PROP_FFT_LOG_BINS,
    PROP_FFT_NUM_SAMPLES,
    PROP_FFT_WINDOW,
    PROP_FFT_HISTORY_COEFF,

    PROP_LAST
};

#define GST_VISUALIZER_MAX(first, second) (((first) > (second)) ? (first) : (second))
#define GST_VISUALIZER_MIN(first, second) (((first) < (second)) ? (first) : (second))

#define GST_VISUALIZER_GLSL_PROFILE GST_GLSL_PROFILE_CORE
#define GST_VISUALIZER_GLSL_VERSION GST_GLSL_VERSION_330

/* pad templates */

/*
static GstStaticPadTemplate gst_visualizer_sink_template =
    GST_STATIC_PAD_TEMPLATE ("sink",
                             GST_PAD_SINK,
                             GST_PAD_ALWAYS,
                             GST_STATIC_CAPS ("audio/x-raw, "
                                                  "format = (string) " GST_AUDIO_NE(S16) ", "
                                 "rate = (int) [ 8000, 96000 ], "
                                 "rate = (int) 44100, "
                                 "channels = (int) 1, "
                                 "layout = (string) interleaved; "
                                 "audio/x-raw, "
                                 "format = (string) " GST_AUDIO_NE(S16) ", "
                                 "rate = (int) [ 8000, 96000 ], "
                                 "channels = (int) 2, "
                                 "channel-mask = (bitmask) 0x3, layout = (string) interleaved")
    );
    */

static GstStaticPadTemplate gst_visualizer_sink_template =
    GST_STATIC_PAD_TEMPLATE ("sink",
         GST_PAD_SINK,
         GST_PAD_ALWAYS,
         GST_STATIC_CAPS ("audio/x-raw, "
             "format = (string) " GST_AUDIO_NE(S16) ", "
             "rate = (int) 44100, "
             "channels = (int) 1")
    );

static GstStaticPadTemplate gst_visualizer_src_template =
    GST_STATIC_PAD_TEMPLATE ("src",
         GST_PAD_SRC,
         GST_PAD_ALWAYS,
         GST_STATIC_CAPS ("video/x-raw(" GST_CAPS_FEATURE_MEMORY_GL_MEMORY "), "
             "format = (string) RGBA, "
             "width = " GST_VIDEO_SIZE_RANGE ", "
             "height = " GST_VIDEO_SIZE_RANGE ", "
             "framerate = " GST_VIDEO_FPS_RANGE ","
             "texture-target = (string) 2D ; "
             "video/x-raw(ANY), "
             "format = (string) RGBA, "
             "width = " GST_VIDEO_SIZE_RANGE ", "
             "height = " GST_VIDEO_SIZE_RANGE ", "
             "framerate = " GST_VIDEO_FPS_RANGE ","
             "texture-target = (string) 2D")
    );

/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstVisualizer, gst_visualizer, GST_TYPE_GL_FILTER,
    GST_DEBUG_CATEGORY_INIT (gst_visualizer_debug_category, "visualizer", 0, "Audio visualizer"))

#define VISUALIZER_DEFAULT_SHADER_TYPE VISUALIZER_SHADER_TYPE_CUSTOM
#define GST_TYPE_VISUALIZER_SHADER_TYPE (gst_visualizer_shader_type_get_type())
static GType gst_visualizer_shader_type_get_type()
{
    static GType visualizerShaderType = 0;
    static const GEnumValue shaderType[] = {
        {VISUALIZER_SHADER_TYPE_NONE, "Unknown shader type", "None"},
        {VISUALIZER_SHADER_TYPE_CUSTOM, "Custom shader type, use provided vertex/fragment shader source code", "Custom"},
        {0, NULL, NULL}
    };

    if (!visualizerShaderType)
    {
        visualizerShaderType = g_enum_register_static("GstVisualizerShaderType", shaderType);
    }

    return visualizerShaderType;
}

#define VISUALIZER_DEFAULT_SAMPLING_TYPE VISUALIZER_SAMPLING_TYPE_NEAREST
#define VISUALIZER_DEFAULT_SAMPLING_VALUE GL_NEAREST
#define GST_TYPE_VISUALIZER_SAMPLING_TYPE (gst_visualizer_sampling_type_get_type())
static GType gst_visualizer_sampling_type_get_type()
{
    static GType visualizerSamplingType = 0;
    static const GEnumValue samplingType[] = {
        {VISUALIZER_SAMPLING_TYPE_NONE, "Unknown sampling type", "None"},
        {VISUALIZER_SAMPLING_TYPE_NEAREST, "Use the nearest value", "Nearest"},
        {VISUALIZER_SAMPLING_TYPE_LINEAR, "Use linear interpolation of values", "Linear"},
        {0, NULL, NULL}
    };

    if (!visualizerSamplingType)
    {
        visualizerSamplingType = g_enum_register_static("GstVisualizerSamplingType", samplingType);
    }

    return visualizerSamplingType;
}

#define VISUALIZER_DEFAULT_EDGE_TYPE VISUALIZER_EDGE_TYPE_CLAMP_TO_BORDER
#define VISUALIZER_DEFAULT_EDGE_VALUE GL_CLAMP_TO_BORDER
#define GST_TYPE_VISUALIZER_EDGE_TYPE (gst_visualizer_edge_type_get_type())
static GType gst_visualizer_edge_type_get_type()
{
    static GType visualizerEdgeType = 0;
    static const GEnumValue edgeType[] = {
        {VISUALIZER_EDGE_TYPE_NONE, "Unknown edge sampling type", "None"},
        {VISUALIZER_EDGE_TYPE_REPEAT, "Repeat the texture", "Repeat"},
        {VISUALIZER_EDGE_TYPE_MIRRORED_REPEAT, "Repeat the texture but mirrored", "RepeatMirrored"},
        {VISUALIZER_EDGE_TYPE_CLAMP_TO_EDGE, "Clamp texture coordinates to the edge", "ClampToEdge"},
        {VISUALIZER_EDGE_TYPE_CLAMP_TO_BORDER, "Clamp the texture coordinates to border and use border color",
            "ClampToBorder"},
        {0, NULL, NULL}
    };

    if (!visualizerEdgeType)
    {
        visualizerEdgeType = g_enum_register_static("GstVisualizerEdgeType", edgeType);
    }

    return visualizerEdgeType;
}

#define VISUALIZER_DEFAULT_WINDOW_TYPE VISUALIZER_WINDOW_TYPE_BLACKMAN
#define VISUALIZER_DEFAULT_WINDOW_VALUE GST_FFT_WINDOW_BLACKMAN
#define GST_TYPE_VISUALIZER_WINDOW_TYPE (gst_visualizer_window_type_get_type())
static GType gst_visualizer_window_type_get_type()
{
    static GType visualizerWindowType = 0;
    static const GEnumValue windowType[] = {
        {VISUALIZER_WINDOW_TYPE_NONE, "Unknown window type", "None"},
        {VISUALIZER_WINDOW_TYPE_RECTANGULAR, "Rectangular window", "Rectangular"},
        {VISUALIZER_WINDOW_TYPE_HAMMING, "Hamming window", "Hamming"},
        {VISUALIZER_WINDOW_TYPE_HANN, "Hanning window", "Hanning"},
        {VISUALIZER_WINDOW_TYPE_BARTLETT, "Bartlett window", "Bartlett"},
        {VISUALIZER_WINDOW_TYPE_BLACKMAN, "Blackman window", "Blackman"},
        {0, NULL, NULL}
    };

    if (!visualizerWindowType)
    {
        visualizerWindowType = g_enum_register_static("GstVisualizerWindowType", windowType);
    }

    return visualizerWindowType;
}

static void gst_visualizer_class_init(GstVisualizerClass * klass)
{
    GObjectClass *gObjectClass = G_OBJECT_CLASS(klass);
    GstBaseTransformClass *baseTransformClass = GST_BASE_TRANSFORM_CLASS(klass);
    GstGLBaseFilterClass *glBaseFilterClass = GST_GL_BASE_FILTER_CLASS(klass);
    GstGLFilterClass *glFilterClass = GST_GL_FILTER_CLASS(klass);
    GstElementClass *elementClass = GST_ELEMENT_CLASS(klass);

    // Replace the sink pad with audio one.
    gst_element_class_add_pad_template(elementClass, gst_static_pad_template_get(&gst_visualizer_src_template));
    gst_element_class_add_pad_template(elementClass, gst_static_pad_template_get(&gst_visualizer_sink_template));

    gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "Visualizer", "Generic", "Audio visualizer.",
      "Tomas Polasek<xpolas34@stud.fit.vutbr.cz>");

    baseTransformClass->transform_caps = gst_visualizer_transform_caps;
    baseTransformClass->set_caps = gst_visualizer_set_caps;
    baseTransformClass->get_unit_size = gst_visualizer_get_unit_size;
    baseTransformClass->propose_allocation = gst_visualizer_propose_allocation;
    baseTransformClass->decide_allocation = gst_visualizer_decide_allocation;
    baseTransformClass->fixate_caps = gst_visualizer_fixate_caps;

    gObjectClass->set_property = gst_visualizer_set_property;
    gObjectClass->get_property = gst_visualizer_get_property;
    gObjectClass->dispose = gst_visualizer_dispose;
    gObjectClass->finalize = gst_visualizer_finalize;

    //glFilterClass->set_caps = gst_visualizer_set_caps;
    // TODO - Choose only one - filter or filter_texture and don't set the other one!
    glFilterClass->filter = gst_visualizer_filter;
    //glFilterClass->filter_texture = gst_visualizer_filter_texture;
    glFilterClass->init_fbo = gst_visualizer_init_fbo;
    glFilterClass->transform_internal_caps = gst_visualizer_transform_internal_caps;

    //glBaseFilterClass->gl_set_caps = gst_visualizer_gl_set_caps;
    glBaseFilterClass->gl_start = gst_visualizer_gl_start;
    glBaseFilterClass->gl_stop = gst_visualizer_gl_stop;
    glBaseFilterClass->supported_gl_api = GST_GL_API_OPENGL3;

    g_object_class_install_property(gObjectClass,
                                    PROP_SHADER_TYPE,
                                    g_param_spec_enum("shader-type",
                                                      "Shader type",
                                                      "Use prepared shader type",
                                                      GST_TYPE_VISUALIZER_SHADER_TYPE,
                                                      VISUALIZER_DEFAULT_SHADER_TYPE,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    // TODO - Add implicit uniform names used by the plugin.
    g_object_class_install_property(gObjectClass,
                                    PROP_VERTEX_SRC,
                                    g_param_spec_string("vertex-source",
                                                        "Vertex shader source code",
                                                        "Source code for the vertex shader. Automatically sets Custom shader type",
                                                        VISUALIZER_DEFAULT_VERT_SOURCE,
                                                        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    // TODO - Add implicit uniform names used by the plugin.
    g_object_class_install_property(gObjectClass,
                                    PROP_FRAGMENT_SRC,
                                    g_param_spec_string("fragment-source",
                                                        "Fragment shader source code",
                                                        "Source code for the fragment shader. Automatically sets Custom shader type",
                                                        VISUALIZER_DEFAULT_FRAG_SOURCE,
                                                        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_UNIFORMS,
                                    g_param_spec_boxed("uniforms",
                                                       "Uniforms set to the shader",
                                                       "Uniform values sent to the GLSL shader",
                                                       GST_TYPE_STRUCTURE,
                                                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FREQ_SAMPLING,
                                    g_param_spec_enum("freq-sampling",
                                                      "Frequency texture sampling mode",
                                                      "Sampling mode of the frequency texture",
                                                      GST_TYPE_VISUALIZER_SAMPLING_TYPE,
                                                      VISUALIZER_DEFAULT_SAMPLING_TYPE,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FREQ_EDGE,
                                    g_param_spec_enum("freq-edge",
                                                      "Frequency texture edge sampling mode",
                                                      "Behavior of sampling of the frequency texture near edges",
                                                      GST_TYPE_VISUALIZER_EDGE_TYPE,
                                                      VISUALIZER_DEFAULT_EDGE_TYPE,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_HISTORY,
                                    g_param_spec_uint("fft-history",
                                                      "Size of frequency history",
                                                      "How many history samples should be kept in the frequency texture. "
                                                          "Rounded to next higher exponent of 2",
                                                      VISUALIZER_MIN_HISTORY,
                                                      VISUALIZER_MAX_HISTORY,
                                                      VISUALIZER_DEFAULT_HISTORY,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_BINS,
                                    g_param_spec_uint("fft-bins",
                                                      "Number of frequency bins",
                                                      "Number of frequency bins in the frequency texture, set to 0 for auto. "
                                                          "Rounded to next higher exponent of 2 or zero",
                                                      VISUALIZER_MIN_BINS,
                                                      VISUALIZER_MAX_BINS,
                                                      VISUALIZER_DEFAULT_BINS,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_NORMALIZE,
                                    g_param_spec_boolean("fft-normalize",
                                                         "Dynamic frequency normalization",
                                                         "Uses dynamic frequency normalization for each row of frequency data. "
                                                             "Else uses static normalization scale",
                                                         VISUALIZER_DEFAULT_NORMALIZE,
                                                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_LOG_BINS,
                                    g_param_spec_boolean("fft-log-bins",
                                                         "Logarithmic frequency bins",
                                                         "Divides frequencies into bins logarithmically, if enabled. "
                                                             "Else uses linear division",
                                                         VISUALIZER_DEFAULT_LOG_BINS,
                                                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_NUM_SAMPLES,
                                    g_param_spec_uint("fft-num-samples",
                                                      "Number of samples used in FFT",
                                                      "How many samples will be used in the FFT",
                                                      VISUALIZER_MIN_NUM_SAMPLES,
                                                      VISUALIZER_MAX_NUM_SAMPLES,
                                                      VISUALIZER_DEFAULT_NUM_SAMPLES,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_WINDOW,
                                    g_param_spec_enum("fft-window",
                                                      "FFT windowing function",
                                                      "Window used before FFT is applied",
                                                      GST_TYPE_VISUALIZER_WINDOW_TYPE,
                                                      VISUALIZER_DEFAULT_WINDOW_TYPE,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    g_object_class_install_property(gObjectClass,
                                    PROP_FFT_HISTORY_COEFF,
                                    g_param_spec_float("fft-history-coeff",
                                                      "FFT history coefficient",
                                                      "How much of the past sample should we keep, when getting a new one",
                                                      VISUALIZER_MIN_HISTORY_COEFF,
                                                      VISUALIZER_MAX_HISTORY_COEFF,
                                                      VISUALIZER_DEFAULT_HISTORY_COEFF,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static gboolean gst_visualizer_gl_start(GstGLBaseFilter *base)
{
    /*
     * Initialize OpenGL resources HERE:
     */

    // Call supers' gl_start.
    return GST_GL_BASE_FILTER_CLASS(gst_visualizer_parent_class)->gl_start(base);
}

static void gst_visualizer_gl_stop(GstGLBaseFilter *base)
{
    GstVisualizer *visualizer = GST_VISUALIZER(base);
    GstGLFuncs *gl = base->context->gl_vtable;

    /*
     * De-initialize OpenGL resources HERE:
     */

    if (visualizer->freq.textureHandle)
    {
        gl->DeleteTextures(1u, &visualizer->freq.textureHandle);
        visualizer->freq.textureHandle = 0u;
    }

    if (visualizer->shader)
    {
        gst_object_unref(visualizer->shader);
        visualizer->shader = NULL;
    }

    // Call supers' gl_stop.
    GST_GL_BASE_FILTER_CLASS(gst_visualizer_parent_class)->gl_stop(base);
}

static gboolean gst_visualizer_set_caps(GstBaseTransform *base, GstCaps *inCaps, GstCaps *outCaps)
{
    GstGLFilter *filter = GST_GL_FILTER(base);
    GstVisualizer *visualizer = GST_VISUALIZER(base);

    GST_DEBUG("InCaps %" GST_PTR_FORMAT, (void*)inCaps);
    if (!gst_audio_info_from_caps(&visualizer->audioInInfo, inCaps))
    {
        GST_WARNING("Wrong caps In, expected audio, got : %" GST_PTR_FORMAT, (void*)inCaps);
        return FALSE;
    }
    GST_DEBUG("OutCaps %" GST_PTR_FORMAT, (void*)outCaps);
    if (!gst_video_info_from_caps(&filter->out_info, outCaps))
    {
        GST_WARNING("Wrong caps Out, expected video, got : %" GST_PTR_FORMAT, (void*)outCaps);
        return FALSE;
    }
    GST_DEBUG("Done!");

    gst_caps_replace(&filter->out_caps, outCaps);

    GST_DEBUG_OBJECT(filter, "set_caps %dx%d in %" GST_PTR_FORMAT " out %" GST_PTR_FORMAT,
                     GST_VIDEO_INFO_WIDTH(&filter->out_info),
                     GST_VIDEO_INFO_HEIGHT(&filter->out_info),
                     (void*)inCaps, (void*)outCaps);

    GstAudioInfo audioInfo;
    gboolean ret = gst_audio_info_from_caps(&audioInfo, inCaps);
    if (ret)
    {
        gst_visualizer_set_freq_texture_info(visualizer, &audioInfo);
    }
    else
    {
        GST_WARNING("Unable to get audio info from %" GST_PTR_FORMAT, (void*)inCaps);
    }

    //return GST_BASE_TRANSFORM_CLASS(gst_visualizer_parent_class)->set_caps(bt, incaps, outcaps);
    return ret;
}

static gboolean gst_visualizer_set_freq_texture_info(GstVisualizer *visualizer, GstAudioInfo *audioInfo)
{
    GST_DEBUG("Setting frequency texture information");

    if (visualizer->freq.fftOutput)
    {
        GST_DEBUG("Freeing fft data buffer");
        g_free(visualizer->freq.fftOutput);
        visualizer->freq.fftOutput = NULL;
        visualizer->freq.fftOutputLength = 0u;
    }
    if (visualizer->freq.fftInstance)
    {
        GST_DEBUG("Freeing fft instance");
        g_free(visualizer->freq.fftInstance);
        visualizer->freq.fftInstance = NULL;
    }
    if (visualizer->freq.sampleAcc)
    {
        GST_DEBUG("Freeing sample accumulator");
        g_free(visualizer->freq.sampleAcc);
        g_free(visualizer->freq.sampleAccW);
        visualizer->freq.sampleAcc = NULL;
        visualizer->freq.sampleAccW = NULL;
        visualizer->freq.sampleAccLength = 0u;
        visualizer->freq.sampleAccSize = 0u;
    }
    if (visualizer->freq.freqTextureBuffer)
    {
        GST_DEBUG("Freeing frequency buffer");
        g_free(visualizer->freq.freqTextureBuffer);
        visualizer->freq.freqTextureBuffer = NULL;
        visualizer->freq.recreateFreqTextureBuffer = TRUE;
    }

    switch (GST_AUDIO_FORMAT_INFO_FORMAT(audioInfo->finfo))
    {
        case GST_AUDIO_FORMAT_S16LE:
        {
            GST_DEBUG("Using S16LE audio format");
            visualizer->audioProcessFun = gst_visualizer_process_audio_S16;
            visualizer->fftDataCreateFun = gst_visualizer_freq_data_S16;
            break;
        }
        default:
        {
            GST_ERROR("Unknown audio format.");
            return FALSE;
        }
    }

    visualizer->bytesPerSample = GST_AUDIO_INFO_BPS(audioInfo);
    // Send only one number - magnitude.
    visualizer->freq.textureInternalFormat = GL_RED;
    visualizer->freq.textureFormat = GL_RED;
    // Input data are floats.
    visualizer->freq.textureType = GL_FLOAT;
    visualizer->freq.recreateTexture = TRUE;
    visualizer->freq.uploadTexture = TRUE;
    visualizer->freq.normalizer = 0u;

    return TRUE;
}

static gboolean gst_visualizer_get_unit_size(GstBaseTransform *base, GstCaps *caps, gsize *size)
{
    GST_DEBUG("GetUnitSize");

    gboolean ret = FALSE;
    GstStructure *structure = gst_caps_get_structure(caps, 0);

    if (gst_structure_has_name(structure, "video/x-raw"))
    {
        GstVideoInfo info;
        ret = gst_video_info_from_caps(&info, caps);
        if (ret)
        {
            *size = GST_VIDEO_INFO_SIZE(&info);
        }
    }
    else if (gst_structure_has_name(structure, "audio/x-raw"))
    {
        GstAudioInfo info;
        ret = gst_audio_info_from_caps(&info, caps);
        if (ret)
        {
            g_assert(GST_AUDIO_INFO_BPF(&info) >= 0);
            *size = GST_AUDIO_INFO_BPF(&info);
        }
    }

    return ret;
}

static gboolean gst_visualizer_propose_allocation(GstBaseTransform *base, GstQuery *decideQuery, GstQuery *query)
{
    GST_DEBUG("ProposeAllocation");

    GstVisualizer *visualizer = GST_VISUALIZER(base);
    GstCaps *caps = NULL;
    gboolean needPool = FALSE;
    gst_query_parse_allocation (query, &caps, &needPool);

    if (!caps)
    {
        GST_DEBUG("No caps specified!");
        return FALSE;
    }
    GstStructure *structure = gst_caps_get_structure(caps, 0);

    if (gst_structure_has_name(structure, "video/x-raw"))
    {
        return gst_visualizer_propose_allocation_video(visualizer, needPool, caps, decideQuery, query);
    }
    else if (gst_structure_has_name(structure, "audio/x-raw"))
    {
        return gst_visualizer_propose_allocation_audio(visualizer, needPool, caps, decideQuery, query);
    }

    GST_DEBUG("Unknown cap type: %" GST_PTR_FORMAT, (void*)caps);
    return FALSE;
}

static gboolean gst_visualizer_propose_allocation_video(GstVisualizer *visualizer, gboolean needPool,
                                                        GstCaps *caps, GstQuery *decideQuery, GstQuery *query)
{
    GST_DEBUG("ProposeAllocationVideo");

    GstGLContext *context = GST_GL_BASE_FILTER(visualizer)->context;
    g_assert(context);

    if (needPool)
    {
        GstBufferPool *pool = NULL;
        GstStructure *config = NULL;
        GstVideoInfo info;

        if (!gst_video_info_from_caps(&info, caps))
        {
            GST_DEBUG("Invalid caps! : %" GST_PTR_FORMAT, (void*)caps);
            return FALSE;
        }

        guint size = info.size;

        GST_DEBUG("Creating a new pool!");
        pool = gst_gl_buffer_pool_new(context);

        config = gst_buffer_pool_get_config(pool);
        gst_buffer_pool_config_set_params(config, caps, size, 0, 0);

        if (!gst_buffer_pool_set_config(pool, config))
        {
            g_object_unref(pool);
            GST_DEBUG("Failed to configure the new pool!");
            return FALSE;
        }

        gst_query_add_allocation_pool(query, pool, size, 1, 0);
        g_object_unref(pool);
    }

    if (context->gl_vtable->FenceSync)
    {
        gst_query_add_allocation_meta(query, GST_GL_SYNC_META_API_TYPE, 0);
    }

    return TRUE;
}

static gboolean gst_visualizer_propose_allocation_audio(GstVisualizer *visualizer, gboolean needPool,
                                                        GstCaps *caps, GstQuery *decideQuery, GstQuery *query)
{
    GST_DEBUG("ProposeAllocationAudio");

    gboolean ret;

    if (decideQuery == NULL)
    {
        GST_DEBUG("Passing through query");
        ret = gst_pad_peer_query(GST_BASE_TRANSFORM(visualizer)->srcpad, query);
    }
    else
    {
        guint numMetas = gst_query_get_n_allocation_metas(decideQuery);

        for (guint iii = 0; iii < numMetas; iii++)
        {
            GType api;
            const GstStructure *params = NULL;

            api = gst_query_parse_nth_allocation_meta (decideQuery, iii, &params);
            GST_DEBUG("Proposing metadata %s", g_type_name(api));
            gst_query_add_allocation_meta(query, api, params);
        }

        ret = TRUE;
    }

    return ret;
}

static gboolean gst_visualizer_decide_allocation(GstBaseTransform *base, GstQuery *query)
{
    GST_DEBUG("DecideAllocation");

    GstVisualizer *visualizer = GST_VISUALIZER(base);
    GstCaps *caps = NULL;
    gst_query_parse_allocation (query, &caps, NULL);

    if (!caps)
    {
        GST_DEBUG("No caps specified!");
        return FALSE;
    }
    GstStructure *structure = gst_caps_get_structure(caps, 0);

    // Get the OpenGL context!
    if (!GST_BASE_TRANSFORM_CLASS(gst_visualizer_parent_class)->decide_allocation(base, query))
    {
        return FALSE;
    }

    if (gst_structure_has_name(structure, "video/x-raw"))
    {
        return gst_visualizer_decide_allocation_video(visualizer, caps, query);
    }
    else if (gst_structure_has_name(structure, "audio/x-raw"))
    {
        return gst_visualizer_decide_allocation_audio(visualizer, caps, query);
    }

    GST_DEBUG("Unknown cap type: %" GST_PTR_FORMAT, (void*)caps);
    return FALSE;
}
static gboolean gst_visualizer_decide_allocation_video(GstVisualizer *visualizer, GstCaps *caps, GstQuery *query)
{
    GST_DEBUG("DecideAllocationVideo");

    GstGLContext *context = GST_GL_BASE_FILTER(visualizer)->context;
    GstBufferPool *pool = NULL;
    guint min = 0;
    guint max = 0;
    guint size = 0;
    gboolean updatePool = FALSE;

    g_assert(context);

    if (gst_query_get_n_allocation_pools(query) > 0)
    {
        gst_query_parse_nth_allocation_pool(query, 0, &pool, &size, &min, &max);

        updatePool = TRUE;
    }
    else
    {
        GstVideoInfo vinfo;

        gst_video_info_init (&vinfo);
        gst_video_info_from_caps (&vinfo, caps);
        size = vinfo.size;
        updatePool = FALSE;
    }

    if (!pool || !GST_IS_GL_BUFFER_POOL(pool))
    {
        if (pool)
        {
            gst_object_unref(pool);
        }
        pool = gst_gl_buffer_pool_new(context);
    }

    GstStructure *config = gst_buffer_pool_get_config(pool);

    gst_buffer_pool_config_set_params(config, caps, size, min, max);
    gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_VIDEO_META);
    if (gst_query_find_allocation_meta(query, GST_GL_SYNC_META_API_TYPE, NULL))
    {
        gst_buffer_pool_config_add_option(config, GST_BUFFER_POOL_OPTION_GL_SYNC_META);
    }

    gst_buffer_pool_set_config(pool, config);

    if (updatePool)
    {
        gst_query_set_nth_allocation_pool(query, 0, pool, size, min, max);
    }
    else
    {
        gst_query_add_allocation_pool(query, pool, size, min, max);
    }

    gst_object_unref(pool);

    return TRUE;
}

static gboolean gst_visualizer_decide_allocation_audio(GstVisualizer *visualizer, GstCaps *caps, GstQuery *query)
{
    GST_ERROR("DecideAllocationAudio");
    return FALSE;
}

static GstCaps *gst_visualizer_fixate_caps(GstBaseTransform *trans, GstPadDirection direction,
                                           GstCaps *caps, GstCaps *otherCaps)
{
    // TODO - Fixate the video caps to some minimal resolution.
    GST_DEBUG("FixateCaps");
    otherCaps = gst_caps_fixate(otherCaps);
    GST_DEBUG("Fixated %" GST_PTR_FORMAT " to %" GST_PTR_FORMAT, (void*)caps, (void*)otherCaps);

    return otherCaps;
}

static inline gboolean gst_visualizer_clock_to_double(GstClockTime time, gdouble *result)
{
    if (!GST_CLOCK_TIME_IS_VALID(time))
    {
        return FALSE;
    }

    *result = (gdouble) time / GST_SECOND;

    return TRUE;
}

static inline gboolean gst_visualizer_time_to_double(gint64 time, gdouble *result)
{
    if (time == -1)
    {
        return FALSE;
    }

    *result = (gdouble) time / GST_USECOND;

    return TRUE;
}

static gboolean gst_visualizer_filter(GstGLFilter *filter, GstBuffer *inBuf, GstBuffer *outBuf)
{
    // Called when new buffer arrives
    GST_DEBUG("Filter");

    GstVisualizer *visualizer = GST_VISUALIZER(filter);
    GST_DEBUG("Got input buffer: PTS %lu DTS %lu", GST_TIME_AS_MSECONDS(GST_BUFFER_PTS(inBuf)), GST_TIME_AS_MSECONDS(GST_BUFFER_DTS(inBuf)));
    GST_DEBUG("Got output buffer: PTS %lu DTS %lu", GST_TIME_AS_MSECONDS(GST_BUFFER_PTS(outBuf)), GST_TIME_AS_MSECONDS(GST_BUFFER_DTS(outBuf)));

    if (!gst_visualizer_clock_to_double(GST_BUFFER_PTS(inBuf), &visualizer->time))
    {
        if (!gst_visualizer_clock_to_double(GST_BUFFER_DTS(inBuf), &visualizer->time))
        {
            gst_visualizer_time_to_double(g_get_monotonic_time(), &visualizer->time);
        }
    }

    if (!gst_visualizer_process_audio(visualizer, inBuf))
    {
        GST_WARNING("Unable to process audio!");
        return FALSE;
    }
    // TODO - Generate the input texture!
    guint inTex = 0;

    GstVideoFrame outFrame;

    if (!gst_video_frame_map(&outFrame, &filter->out_info, outBuf, GST_MAP_WRITE | GST_MAP_GL))
    {
        GST_WARNING("Unable to map output buffer as OpenGL memory!");
        return FALSE;
    }

    GstMemory *outTex = outFrame.map[0].memory;
    if (!gst_is_gl_memory(outTex))
    {
        gst_video_frame_unmap(&outFrame);
        GST_WARNING("Output texture is NOT OpenGL memory!");
        return FALSE;
    }

    GST_DEBUG ("Calling filter_texture with textures in:%i out:%i",
               inTex,
               GST_GL_MEMORY_CAST(outTex)->tex_id);

    gboolean ret = gst_visualizer_filter_texture(filter, inTex, GST_GL_MEMORY_CAST(outTex));

    gst_video_frame_unmap (&outFrame);

    return ret;
}

static gboolean gst_visualizer_check_create_freq_data(GstVisualizer *visualizer)
{
    gboolean res = TRUE;

    gsize numFreqs = gst_visualizer_calc_num_freqs(visualizer, visualizer->numSamples);

    if (visualizer->freq.fftOutputLength != numFreqs || visualizer->freq.sampleAccLength != visualizer->numSamples)
    {
        GST_DEBUG("Recreating frequency data buffer and sample accumulator");
        g_assert(visualizer->fftDataCreateFun);
        res = visualizer->fftDataCreateFun(visualizer, numFreqs, visualizer->numSamples);
        if (res)
        {
            visualizer->freq.fftOutputLength = numFreqs;
            visualizer->freq.sampleAccLength = visualizer->numSamples;
            visualizer->freq.sampleAccSize = 0u;
        }
        else
        {
            visualizer->freq.fftOutputLength = 0u;
            visualizer->freq.sampleAccLength = 0u;
            visualizer->freq.sampleAccSize = 0u;
        }
    }

    return res;
}

static gboolean gst_visualizer_check_create_freq_buffer(GstVisualizer *visualizer)
{
    // Special value 0 means we should automatically set the number of bins.
    if (visualizer->freq.freqTextureBufferWidth == 0u)
    {
        gsize autoBins = visualizer->freq.fftOutputLength;
        g_assert(autoBins == gst_visualizer_calc_num_freqs(visualizer, visualizer->freq.sampleAccLength));

        GST_DEBUG("Automatically setting bins to %lu", autoBins);

        if (visualizer->freq.freqTextureBufferWidth != autoBins)
        { // If the number of bins should be changed, also recreate the buffer with new size.
            visualizer->freq.freqTextureBufferWidth = autoBins;
            visualizer->freq.recreateFreqTextureBuffer = TRUE;
        }
    }

    if (!visualizer->freq.freqTextureBuffer || visualizer->freq.recreateFreqTextureBuffer)
    {
        GST_DEBUG("Recreating frequency texture buffer");

        // Free old buffer.
        if (visualizer->freq.freqTextureBuffer)
        {
            g_free(visualizer->freq.freqTextureBuffer);
            visualizer->freq.freqTextureBuffer = NULL;
        }

        // Allocate new buffer.
        gsize bufferElements = visualizer->freq.freqTextureBufferWidth * visualizer->freq.freqTextureBufferHeight;
        GST_DEBUG("Allocating frequency texture buffer : %lu elements", bufferElements);
        visualizer->freq.freqTextureBuffer = g_new(GLfloat, bufferElements);
        if (!visualizer->freq.freqTextureBuffer)
        {
            GST_ERROR("Unable to allocate frequency texture buffer");
            return FALSE;
        }

        // Reset values to default.
        memset(visualizer->freq.freqTextureBuffer,
               0,
               sizeof(GLfloat) * bufferElements);

        visualizer->freq.recreateFreqTextureBuffer = FALSE;
    }

    return TRUE;
}

static gsize gst_visualizer_calc_num_freqs(GstVisualizer *visualizer, gsize numSamples)
{
    return numSamples / 2 + 1;
}

static gboolean gst_visualizer_process_audio(GstVisualizer *visualizer, GstBuffer *inBuf)
{
    GST_DEBUG("ProcessAudio");

    GstMapInfo mapInfo;

    // Map the audio data.
    if (!gst_buffer_map(inBuf, &mapInfo, GST_MAP_READ))
    {
        GST_WARNING("Unable to map input buffer!");
        return FALSE;
    }

    // Calculate real number of samples in the input buffer.
    g_assert(visualizer->bytesPerSample > 0);
    gsize currentNumSamples = mapInfo.size / visualizer->bytesPerSample;

    // Return value for this function.
    gboolean ret = FALSE;
    GST_OBJECT_LOCK(visualizer);
    { // Locking Visualizer.
        /* Frequency data structures need to be created first,
         *   since they compute how wide should the frequency texture data array be.
         */

        // Create frequency data buffer used as destination by audio processing and sample accumulator.
        if (!gst_visualizer_check_create_freq_data(visualizer))
        {
            GST_WARNING("Unable to create frequency data structures");
            return FALSE;
        }

        // Create frequency buffer used for storing frequency texture data.
        if (!gst_visualizer_check_create_freq_buffer(visualizer))
        {
            GST_WARNING("Unable to create frequency buffer");
            return FALSE;
        }

        // Process the data using function based on data format.
        g_assert(visualizer->audioProcessFun);
        ret = visualizer->audioProcessFun(visualizer, mapInfo.data, currentNumSamples);
    } // Unlocking Visualizer.
    GST_OBJECT_UNLOCK(visualizer);

    gst_buffer_unmap(inBuf, &mapInfo);

    return ret;
}

static void gst_visualizer_shift_freq_buffer(GstVisualizer *visualizer)
{
    if (visualizer->freq.freqTextureBufferHeight > 1u)
    {
        GST_DEBUG("Shifting frequency texture by one row : height %lu", visualizer->freq.freqTextureBufferHeight);

        GLfloat *buffer = visualizer->freq.freqTextureBuffer;
        memmove(buffer + visualizer->freq.freqTextureBufferWidth,
                buffer,
                sizeof(*buffer) * visualizer->freq.freqTextureBufferWidth * (visualizer->freq.freqTextureBufferHeight - 1u));
    }
}

static gboolean gst_visualizer_process_audio_S16(GstVisualizer *visualizer, void *data, gsize numSamples)
{
    GST_DEBUG("ProcessAudioS16");

    if (!data)
    {
        GST_WARNING("Received NULL pointer for data!");
        return FALSE;
    }

    if (!visualizer->freq.fftInstance)
    {
        GST_WARNING("FFT instance is not created!");
        return FALSE;
    }

    GST_DEBUG("Got %lu samples!", numSamples);

    gsize accumulated = visualizer->freq.sampleAccSize;
    gsize accSize = visualizer->freq.sampleAccLength;
    gint16 *acc = visualizer->freq.sampleAcc;
    gint16 *accW = visualizer->freq.sampleAccW;
    gint16 *inBuffer = data;
    GstFFTS16 *fft = visualizer->freq.fftInstance;
    GstFFTS16Complex *outBuffer = visualizer->freq.fftOutput;
    gsize numFreqs = visualizer->freq.fftOutputLength;

    // Accumulate newly acquired samples and process if needed.
    for (gsize currentSample = 0u; currentSample < numSamples;)
    {
        // Calculate number of elements which will be moved, maximum number of size of accumulator
        gsize toMove = GST_VISUALIZER_MIN(numSamples - currentSample, accSize - accumulated);

        GST_DEBUG("At %lu ; moving %lu / %lu (max %lu); accumulated %lu",
                  currentSample, toMove, numSamples, accSize - accumulated, accumulated);

        // Move elements to accumulator.
        memcpy(acc + accumulated, inBuffer + currentSample, toMove);
        currentSample += toMove;
        accumulated += toMove;

        GST_DEBUG("Accumulated %lu / %lu", accumulated, accSize);

        // Check if we filled the accumulator.
        if (accumulated == accSize)
        { // We need to run FFT!
            GST_DEBUG("Running FFT");

            // Copy accumulator to temporary buffer, so we can run windowing function on it and keep the original.
            memcpy(accW, acc, accSize);

            // Use windowing function to smooth out the output.
            gst_fft_s16_window(fft, accW, visualizer->freq.windowType);

            // Perform FFT.
            gst_fft_s16_fft(fft, accW, outBuffer);

            GST_DEBUG("Generated %lu frequency samples.", numFreqs);

            // Make space for new data.
            gst_visualizer_shift_freq_buffer(visualizer);

            // TODO - Redo...
            visualizer->freq.normalizer = GST_VISUALIZER_MAX(visualizer->freq.normalizer, 4.0f);
            // Maximum for next normalizer value.
            float max = 0.0f;
            // Buffer used as target for resolving.
            GLfloat *resolveBuffer = visualizer->freq.freqTextureBuffer;
            // Resolve generated frequency samples into new row of frequency texture.
            for (gsize iii = 0; iii < visualizer->freq.freqTextureBufferWidth; ++iii)
            {
                float mag = 0.0f;
                if (visualizer->freq.freqTextureBufferWidth != numFreqs)
                {
                    gsize start = 0u;
                    gsize end = 0u;
                    /*
                    float modifier = powf(2.0, log2f(numFreqs) - visualizer->freq.freqTextureBufferWidth);

                    // TODO - Check it.
                    if (visualizer->logBins)
                    {
                        // TODO - Does not work!
                        start = (1u << iii) * modifier;
                        end = GST_VISUALIZER_MIN((1u << (iii + 1u)) * modifier, numFreqs);
                    }
                    else
                    {
                        start = iii * numFreqs / visualizer->freq.freqTextureBufferWidth;
                        end = GST_VISUALIZER_MAX((iii + 1u) * numFreqs / visualizer->freq.freqTextureBufferWidth, visualizer->freq.freqTextureBufferWidth - 1u);
                    }
                     */

                    if (visualizer->logBins)
                    {
                        float base = 2.0f;
                        start = powf(base, iii) - 1.0f;
                        end = powf(base, iii + 1) - 1.0f;
                    }
                    else
                    {
                        start = iii * numFreqs / visualizer->freq.freqTextureBufferWidth;
                        end = (iii + 1) * numFreqs / visualizer->freq.freqTextureBufferWidth;
                    }

                    GST_ERROR("iii: %lu ; start: %lu ; end: %lu", iii, start, end);

                    if (start < visualizer->freq.freqTextureBufferWidth)
                    {
                        end = MIN(end, visualizer->freq.freqTextureBufferWidth);
                        for (gsize jjj = start; jjj < end; ++jjj)
                        {
                            mag += sqrtf(outBuffer[jjj].r * outBuffer[jjj].r + outBuffer[jjj].i * outBuffer[jjj].i);
                        }
                    }
                }
                else
                {
                    // Magnitude of the complex frequency produced by FFT.
                    mag = sqrtf(outBuffer[iii].r * outBuffer[iii].r + outBuffer[iii].i * outBuffer[iii].i);
                }

                //float freqCoeff = expf(1.0f + ((float)iii) / visualizer->freq.freqTextureBufferWidth);
                float normalFreq = (float)iii / visualizer->freq.freqTextureBufferWidth;
                float freqSquared = normalFreq * normalFreq;
                float freqCubed = freqSquared * normalFreq;
                float freqCoeff = (3.0f * freqSquared - 2.0f * freqCubed + 0.5f);

                mag *= freqCoeff * freqCoeff;

                if (visualizer->freqNormalize)
                { // Normalize the calculated frequency.
                    //resolveBuffer[iii] = mag / (visualizer->freq.normalizer * expf(-0.002f * iii));
                    //resolveBuffer[iii] = mag / (visualizer->freq.normalizer * expf(0.2f * iii));
                    resolveBuffer[iii] = mag / visualizer->freq.normalizer;
                    max = GST_VISUALIZER_MAX(max, mag);
                }
                else
                { // Pass without normalization.
                    //resolveBuffer[iii] = mag / numSamples;
                    resolveBuffer[iii] = mag;
                }

                if (visualizer->freq.historyCoeff > VISUALIZER_MIN_HISTORY_COEFF)
                { // How much of the historical value should we keep?
                    /*
                    resolveBuffer[iii] = visualizer->freq.historyCoeff * resolveBuffer[iii] +
                                         (1.0f - visualizer->freq.historyCoeff) * resolveBuffer[iii + visualizer->freq.freqTextureBufferWidth];
                                         */
                    resolveBuffer[iii] = visualizer->freq.historyCoeff * resolveBuffer[iii] +
                                         (1.0f - visualizer->freq.historyCoeff) * resolveBuffer[iii + visualizer->freq.freqTextureBufferWidth] +
                                         0.2f * (-visualizer->freq.historyCoeff) * resolveBuffer[iii + visualizer->freq.freqTextureBufferWidth];
                }
            }
            // TODO - Refactor out.
            visualizer->freq.normalizer = 0.8f * visualizer->freq.normalizer + 0.2f * max;
            visualizer->freq.normalizer = GST_VISUALIZER_MIN(GST_VISUALIZER_MAX(visualizer->freq.normalizer, 4.0f), 8.0f);

            GST_DEBUG("New normalizer %f", visualizer->freq.normalizer);

            // Reset accumulator empty.
            accumulated = 0u;
            // We changed the data -> re-upload to GPU.
            visualizer->freq.uploadTexture = TRUE;
        }
    }

    // Keep accumulated samples for next time!
    visualizer->freq.sampleAccSize = accumulated;

    /*
    GstFFTS16Complex *outBuffer = visualizer->freq.fftData;

    GstFFTS16 *fft = gst_fft_s16_new(numSamples, FALSE);
    if (!fft)
    {
        GST_WARNING("Unable to create FFT instance!");
        return FALSE;
    }

    gsize numFreqs = visualizer->freq.freqSamples;

    gst_fft_s16_window(fft, data, GST_FFT_WINDOW_BLACKMAN);
    gst_fft_s16_fft(fft, data, outBuffer);

    GST_DEBUG("Generated %lu frequency samples.", numFreqs);

    gst_fft_s16_free(fft);
     */

    /*
    float scale = 0.0f;
    for (gsize iii = 0; iii < numSamples; ++iii)
    {
        gint16 val = ((gint16*)data)[iii];
        scale += val * val;
    }
    scale /= (float)numSamples;

    if (visualizer->freq.normalizer == 0.0f && visualizer->freqNormalize)
    {
        visualizer->freq.normalizer = 1.0f;
    }
     */

    /*
    // Maximum for next normalizer value.
    float max = 0.0f;
    GLfloat *resolveBuffer = visualizer->freq.freqBuffer;
    for (gsize iii = 0; iii < visualizer->freq.bins; ++iii)
    {
        float mag = 0.0f;
        if (visualizer->freq.bins != numFreqs)
        {
            gsize start = 0u;
            gsize end = 0u;

            // TODO - Check it.
            if (visualizer->logBins)
            {
                start = 1u << iii;
                end = GST_VISUALIZER_MIN(1u << iii, numFreqs - 1u);
            }
            else
            {
                start = iii * numFreqs / visualizer->freq.bins;
                end = GST_VISUALIZER_MAX((iii + 1u) * numFreqs / visualizer->freq.bins, visualizer->freq.bins - 1u);
            }
            GST_DEBUG("iii: %lu ; start: %lu ; end: %lu", iii, start, end);

            for (gsize jjj = start; jjj < end; ++jjj)
            {
                mag += sqrtf(outBuffer[jjj].r * outBuffer[jjj].r + outBuffer[jjj].i * outBuffer[jjj].i);
            }
        }
        else
        {
            mag = sqrtf(outBuffer[iii].r * outBuffer[iii].r + outBuffer[iii].i * outBuffer[iii].i);
        }

        if (visualizer->freqNormalize)
        {
            resolveBuffer[iii] = mag / visualizer->freq.normalizer;
            max = GST_VISUALIZER_MAX(max, mag);
        }
        else
        {
            resolveBuffer[iii] = mag / numSamples;
        }
    }
    visualizer->freq.normalizer = 0.2f * visualizer->freq.normalizer + 0.8f * max;
     */

    /*
    GLfloat *resolveBuffer = visualizer->freq.freqBuffer;
    for (gsize jjj = 0; jjj < visualizer->freq.history; ++jjj)
    {
        for (gsize iii = 0; iii < visualizer->freq.bins; ++iii)
        {
            resolveBuffer[jjj * visualizer->freq.bins + iii] = 0.5f;
        }
    }
     */

    /*
    FILE *f = fopen("fft.txt", "a");
#if 0
    fprintf(f, "timeScale\n");
    for (gsize iii = 0; iii < visualizer->freq.bins; ++iii)
    {
        fprintf(f, "%lu; %f;\n", iii, resolveBuffer[iii]);
    }
    fprintf(f, "time\n");
    for (gsize iii = 0; iii < numSamples; ++iii)
    {
        fprintf(f, "%lu; %d;\n", iii, ((gint16*)data)[iii]);
    }
    fprintf(f, "freq\n");
#endif
    for (gsize iii = 0; iii < visualizer->freq.bins; ++iii)
    {
        fprintf(f, "%lu; %f;\n", iii, sqrt(outBuffer[iii].r * outBuffer[iii].r + outBuffer[iii].i * outBuffer[iii].i));
    }
    fprintf(f, "\n");
    fclose(f);
     */

    return TRUE;
}

static gboolean gst_visualizer_freq_data_S16(GstVisualizer *visualizer, gsize numFreqs, gsize numSamples)
{
    GST_INFO("Allocating S16 frequency buffer : freqs %lu; samples %lu", numFreqs, numSamples);

    if (visualizer->freq.sampleAcc)
    {
        g_free(visualizer->freq.sampleAcc);
        g_free(visualizer->freq.sampleAccW);
        visualizer->freq.sampleAcc = NULL;
        visualizer->freq.sampleAccW = NULL;
    }
    if (visualizer->freq.fftOutput)
    {
        g_free(visualizer->freq.fftOutput);
        visualizer->freq.fftOutput = NULL;
    }
    if (visualizer->freq.fftInstance)
    {
        g_free(visualizer->freq.fftInstance);
        visualizer->freq.fftInstance = NULL;
    }

    // Allocate buffers.
    visualizer->freq.fftOutput = g_new(GstFFTS16Complex, numFreqs);
    visualizer->freq.sampleAcc = g_new(gint16, numSamples);
    visualizer->freq.sampleAccW = g_new(gint16, numSamples);

    // Create FFT instance.
    visualizer->freq.fftInstance = gst_fft_s16_new(numSamples, FALSE);

    if (!visualizer->freq.fftOutput || !visualizer->freq.sampleAcc || !visualizer->freq.fftInstance)
    {
        return FALSE;
    }

    // Initialize buffers to 0.
    memset(visualizer->freq.fftOutput, 0, sizeof(GstFFTS16Complex) * numFreqs);
    memset(visualizer->freq.sampleAcc, 0, sizeof(gint16) * numSamples);

    return TRUE;
}

struct GLCallbackStruct
{
    GstGLFilter *filter;
    guint inTex;
    gpointer data;
};

static gboolean gst_visualizer_framebuffer_func(gpointer data)
{
    struct GLCallbackStruct *glcb = data;
    return gst_visualizer_draw_callback(glcb->filter, glcb->inTex, glcb->data);
}

static gboolean gst_visualizer_filter_texture(GstGLFilter *filter, guint inTex, GstGLMemory *outTex)
{
    // Called when new input/output textures arrive.
    GST_DEBUG("FilterTexture");

    //GstVisualizer *visualizer = GST_VISUALIZER(filter);

    //gst_gl_filter_render_to_target(filter, inTex, outTex, gst_visualizer_hcallback, NULL);

    struct GLCallbackStruct glcb;
    glcb.filter = filter;
    glcb.inTex = inTex;
    glcb.data = NULL;

    return gst_gl_framebuffer_draw_to_texture(filter->fbo, outTex, gst_visualizer_framebuffer_func, &glcb);
}

static gboolean gst_visualizer_init_fbo(GstGLFilter *filter)
{
    // Initialization after FBO is created.

    return TRUE;
}

static GstCaps *gst_visualizer_transform_internal_caps(GstGLFilter *filter, GstPadDirection direction, GstCaps *caps, GstCaps *filter_caps)
{
    GST_DEBUG("Transform_internal_caps");
    switch (direction)
    {
        case GST_PAD_SRC:
        {
            return gst_static_pad_template_get_caps(&gst_visualizer_sink_template);
        }

        case GST_PAD_SINK:
        {
            return gst_static_pad_template_get_caps(&gst_visualizer_src_template);
        }

        default:
        {
            break;
        }
    }

    GST_ERROR("Unknown direction in transform_internal_caps!");
    return NULL;
}

static gboolean gst_visualizer_draw_callback(GstGLFilter *filter, guint inTex, gpointer stuff)
{
    // Rendering callback.
    GST_DEBUG("RenderingCallback");

    GstVisualizer *visualizer = GST_VISUALIZER(filter);
    GstGLFuncs *gl = GST_GL_BASE_FILTER(filter)->context->gl_vtable;
    GstGLShader *shader = gst_visualizer_recompile_shader(visualizer);

    if (!shader)
    {
        GST_ERROR("Unable to compile shader program! Falling back to default.");

        GST_OBJECT_LOCK(visualizer);
        {
            // Set the default sources.
            if (visualizer->vertexSrc)
            {
                g_string_free(visualizer->vertexSrc, TRUE);
            }
            visualizer->vertexSrc = g_string_new(VISUALIZER_DEFAULT_VERT_SOURCE);
            if (visualizer->fragmentSrc)
            {
                g_string_free(visualizer->fragmentSrc, TRUE);
            }
            visualizer->fragmentSrc = g_string_new(VISUALIZER_DEFAULT_FRAG_SOURCE);

            // Set the re-create flag.
            visualizer->recreateShader = TRUE;
        }
        GST_OBJECT_UNLOCK(visualizer);
        // Try to re-create again.
        shader = gst_visualizer_recompile_shader(visualizer);

        if (!shader)
        { // Fallback failed too...
            GST_ERROR("Unable to complete the fallback shader program, failure is inevitable!");
            return FALSE;
        }
    }

    if (!gst_visualizer_update_freq_texture(visualizer))
    {
        GST_ERROR("Unable to update frequency texture!");
        return FALSE;
    }

    gl->ClearColor(1.0, 0.0, 1.0, 1.0);
    gl->Clear(GL_COLOR_BUFFER_BIT);

    gst_gl_shader_use(shader);

    filter->draw_attr_position_loc = gst_gl_shader_get_attribute_location(shader, "vertPos");
    filter->draw_attr_texture_loc = gst_gl_shader_get_attribute_location(shader, "vertUv");
    GST_DEBUG("Position %d, Texture coord %d", filter->draw_attr_position_loc, filter->draw_attr_texture_loc);

    gst_gl_shader_set_uniform_1f(shader, "width", GST_VIDEO_INFO_WIDTH(&filter->out_info));
    gst_gl_shader_set_uniform_1f(shader, "height", GST_VIDEO_INFO_HEIGHT(&filter->out_info));
    gst_gl_shader_set_uniform_1f(shader, "freqWidth", visualizer->freq.freqTextureBufferWidth);
    gst_gl_shader_set_uniform_1f(shader, "freqHeight", visualizer->freq.freqTextureBufferHeight);
    gst_gl_shader_set_uniform_1f(shader, "time", visualizer->time);
    GST_DEBUG("Width %d, Height %d, Time %f",
              GST_VIDEO_INFO_WIDTH(&filter->out_info),
              GST_VIDEO_INFO_HEIGHT(&filter->out_info),
              visualizer->time);

    gst_visualizer_set_custom_uniforms(visualizer);

    gl->ActiveTexture(GL_TEXTURE0);
    // TODO - Use inTex .
    gl->BindTexture(GL_TEXTURE_2D, visualizer->freq.textureHandle);
    GST_DEBUG("Texture %d", visualizer->freq.textureHandle);
    gst_gl_shader_set_uniform_1i(shader, "freqs", 0u);

    gst_gl_filter_draw_fullscreen_quad(filter);
    GST_DEBUG("Finished drawing...");

    gl->BindTexture(GL_TEXTURE_2D, 0u);

    return TRUE;
}

static gboolean gst_visualizer_check_create_freq_texture(GstVisualizer *visualizer)
{
    GstGLFuncs *gl = GST_GL_BASE_FILTER(visualizer)->context->gl_vtable;
    if (visualizer->freq.textureHandle == 0 || visualizer->freq.recreateTexture)
    {
        GST_DEBUG("Creating texture");
        if (visualizer->freq.textureHandle)
        {
            GST_DEBUG("Destroying last texture %u", visualizer->freq.textureHandle);
            gl->DeleteTextures(1u, &visualizer->freq.textureHandle);
            visualizer->freq.textureHandle = 0u;
        }

        gl->GenTextures(1u, &visualizer->freq.textureHandle);
        if (visualizer->freq.textureHandle == 0u)
        {
            GST_WARNING("glGenTextures failed!");
            return FALSE;
        }

        gl->BindTexture(GL_TEXTURE_2D, visualizer->freq.textureHandle);

        gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, visualizer->freq.textureSampling);
        gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, visualizer->freq.textureSampling);
        gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, visualizer->freq.textureEdgeSampling);
        gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, visualizer->freq.textureEdgeSampling);

        gl->BindTexture(GL_TEXTURE_2D, 0u);
    }

    visualizer->freq.recreateTexture = FALSE;
    // TODO - Set freq.uploadTexture to TRUE?

    return TRUE;
}

static gboolean gst_visualizer_update_freq_texture(GstVisualizer *visualizer)
{
    GstGLFuncs *gl = GST_GL_BASE_FILTER(visualizer)->context->gl_vtable;

    if (!gst_visualizer_check_create_freq_texture(visualizer))
    {
        GST_WARNING("Unable to create frequency texture!");
        return FALSE;
    }

    if (visualizer->freq.uploadTexture)
    {
        GST_DEBUG("Recreating texture with fresh data");

        gl->BindTexture(GL_TEXTURE_2D, visualizer->freq.textureHandle);

        GST_DEBUG("Uploading new frequency data : width %lu ; height %lu ; "
                      "internalFormat %d ; format %d ; type %d",
                  visualizer->freq.freqTextureBufferWidth, visualizer->freq.freqTextureBufferHeight,
                  visualizer->freq.textureInternalFormat, visualizer->freq.textureFormat,
                  visualizer->freq.textureType);

        g_assert(visualizer->freq.freqTextureBuffer);

        glTexImage2D(GL_TEXTURE_2D,
                     0, // LOD level - unused.
                     visualizer->freq.textureInternalFormat,
                     visualizer->freq.freqTextureBufferWidth,
                     visualizer->freq.freqTextureBufferHeight,
                     0, // Border must be 0.
                     visualizer->freq.textureFormat,
                     visualizer->freq.textureType,
                     visualizer->freq.freqTextureBuffer);


        GST_DEBUG("Finished upload of frequency data");

        gl->BindTexture(GL_TEXTURE_2D, 0);

        visualizer->freq.uploadTexture = FALSE;
    }

    return TRUE;
}

static GstCaps *gst_visualizer_transform_caps(GstBaseTransform *bt, GstPadDirection direction, GstCaps *caps, GstCaps *filterCaps)
{
    GstGLFilter *filter = GST_GL_FILTER(bt);
    GstCaps *tmp = NULL;
    GstCaps *result = NULL;

    if (gst_base_transform_is_passthrough(bt))
    {
        tmp = gst_caps_ref(caps);
    }
    else
    {
        tmp = GST_GL_FILTER_GET_CLASS(filter)->transform_internal_caps(filter, direction, caps, NULL);

        //gst_caps_unref(tmp);
        result = tmp;
        GST_DEBUG("got caps: %" GST_PTR_FORMAT, (void*)result);
    }

    if (filterCaps)
    {
        GST_DEBUG("filterCaps!");
        //result = gst_caps_intersect_full(filterCaps, tmp, GST_CAPS_INTERSECT_FIRST);
        //gst_caps_unref(tmp);
    }
    else
    {
        result = tmp;
    }

    GST_DEBUG("returning caps: %" GST_PTR_FORMAT, (void*)result);

    return result;
}

static void gst_visualizer_set_shader_type(GstVisualizer *visualizer, GstVisualizerShaderType type)
{
    GST_DEBUG("SetShaderType %s", g_enum_to_string(GST_TYPE_VISUALIZER_SHADER_TYPE, type));

    gboolean valid = TRUE;

    // TODO - Implement!
    switch (type)
    {
        case VISUALIZER_SHADER_TYPE_CUSTOM:
        {
            GST_WARNING("Using custom shader");
            break;
        }
        default:
        {
            GST_WARNING("Unknown shader type");
            valid = FALSE;
            break;
        }
    }

    if (valid)
    {
        visualizer->recreateShader = TRUE;
        visualizer->shaderType = type;
    }
}

static void gst_visualizer_set_sampling(GstVisualizer *visualizer, GstVisualizerSamplingType type)
{
    GST_DEBUG("SetSampling %s", g_enum_to_string(GST_TYPE_VISUALIZER_SAMPLING_TYPE, type));

    gboolean valid = TRUE;

    switch (type)
    {
        case VISUALIZER_SAMPLING_TYPE_NEAREST:
        {
            visualizer->freq.textureSampling = GL_NEAREST;
            break;
        }
        case VISUALIZER_SAMPLING_TYPE_LINEAR:
        {
            visualizer->freq.textureSampling = GL_LINEAR;
            break;
        }
        default:
        {
            GST_WARNING("Unknown sampling value");
            valid = FALSE;
            break;
        }
    }

    if (valid)
    {
        visualizer->freq.recreateTexture = TRUE;
        visualizer->freqSampling = type;
    }
}

static void gst_visualizer_set_window(GstVisualizer *visualizer, GstVisualizerWindowType type)
{
    gboolean valid = TRUE;

    switch (type)
    {
        case VISUALIZER_WINDOW_TYPE_RECTANGULAR:
        {
            visualizer->freq.windowType = GST_FFT_WINDOW_RECTANGULAR;
            break;
        }
        case VISUALIZER_WINDOW_TYPE_HAMMING:
        {
            visualizer->freq.windowType = GST_FFT_WINDOW_HAMMING;
            break;
        }
        case VISUALIZER_WINDOW_TYPE_HANN:
        {
            visualizer->freq.windowType = GST_FFT_WINDOW_HANN;
            break;
        }
        case VISUALIZER_WINDOW_TYPE_BARTLETT:
        {
            visualizer->freq.windowType = GST_FFT_WINDOW_BARTLETT;
            break;
        }
        case VISUALIZER_WINDOW_TYPE_BLACKMAN:
        {
            visualizer->freq.windowType = GST_FFT_WINDOW_BLACKMAN;
            break;
        }
        default:
        {
            GST_WARNING("Unknown FFT window type");
            valid = FALSE;
            break;
        }
    }

    if (valid)
    {
        visualizer->windowType = type;
    }
}

static void gst_visualizer_set_edge(GstVisualizer *visualizer, GstVisualizerEdgeType type)
{
    GST_DEBUG("SetEdge %s", g_enum_to_string(GST_TYPE_VISUALIZER_EDGE_TYPE, type));

    gboolean valid = TRUE;

    switch (type)
    {
        case VISUALIZER_EDGE_TYPE_REPEAT:
        {
            visualizer->freq.textureEdgeSampling = GL_REPEAT;
            break;
        }
        case VISUALIZER_EDGE_TYPE_MIRRORED_REPEAT:
        {
            visualizer->freq.textureEdgeSampling = GL_MIRRORED_REPEAT;
            break;
        }
        case VISUALIZER_EDGE_TYPE_CLAMP_TO_EDGE:
        {
            visualizer->freq.textureEdgeSampling = GL_CLAMP_TO_EDGE;
            break;
        }
        case VISUALIZER_EDGE_TYPE_CLAMP_TO_BORDER:
        {
            visualizer->freq.textureEdgeSampling = GL_CLAMP_TO_BORDER;
            break;
        }
        default:
        {
            GST_WARNING("Unknown edge sampling value");
            valid = FALSE;
            break;
        }
    }

    if (valid)
    {
        visualizer->freq.recreateTexture = TRUE;
        visualizer->freqEdge = type;
    }
}

static guint next_power_of_two(guint num)
{
    // Source : https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
    unsigned int v = num;

    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;

    return v;
}

static void gst_visualizer_set_property(GObject *object, guint propId, const GValue *value, GParamSpec *pSpec)
{
    GST_DEBUG("SetProperty %s", G_PARAM_SPEC_TYPE_NAME(pSpec));

    GstVisualizer *visualizer = GST_VISUALIZER(object);

    switch (propId)
    {
        case PROP_SHADER_TYPE:
        {
            GST_OBJECT_LOCK(visualizer);
            gst_visualizer_set_shader_type(visualizer, g_value_get_enum(value));
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_VERTEX_SRC:
        {
            GST_OBJECT_LOCK(visualizer);
            if (visualizer->vertexSrc)
            {
                g_string_free(visualizer->vertexSrc, TRUE);
            }
            visualizer->vertexSrc = g_string_new(g_value_get_string(value));
            gst_visualizer_set_shader_type(visualizer, VISUALIZER_SHADER_TYPE_CUSTOM);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FRAGMENT_SRC:
        {
            GST_OBJECT_LOCK(visualizer);
            if (visualizer->fragmentSrc)
            {
                g_string_free(visualizer->fragmentSrc, TRUE);
            }
            visualizer->fragmentSrc = g_string_new(g_value_get_string(value));
            gst_visualizer_set_shader_type(visualizer, VISUALIZER_SHADER_TYPE_CUSTOM);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_UNIFORMS:
        {
            GST_OBJECT_LOCK(visualizer);
            if (visualizer->uniforms)
            {
                gst_structure_free(visualizer->uniforms);
            }
            visualizer->uniforms = g_value_dup_boxed(value);
            // TODO - Flag for re-sending new uniforms?
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FREQ_SAMPLING:
        {
            GST_OBJECT_LOCK(visualizer);
            gst_visualizer_set_sampling(visualizer, g_value_get_enum(value));
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FREQ_EDGE:
        {
            GST_OBJECT_LOCK(visualizer);
            gst_visualizer_set_edge(visualizer, g_value_get_enum(value));
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_HISTORY:
        {
            GST_OBJECT_LOCK(visualizer);
            guint newHistory = next_power_of_two(g_value_get_uint(value));
            if (newHistory >= VISUALIZER_MIN_HISTORY && newHistory <= VISUALIZER_MAX_HISTORY)
            {
                visualizer->freq.freqTextureBufferHeight = newHistory;
                visualizer->freqHistorySize = newHistory;
                visualizer->freq.recreateFreqTextureBuffer = TRUE;
            }
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_BINS:
        {
            GST_OBJECT_LOCK(visualizer);
            guint newBins = next_power_of_two(g_value_get_uint(value));
            if (newBins >= VISUALIZER_MIN_BINS && newBins <= VISUALIZER_MAX_BINS)
            {
                visualizer->freq.freqTextureBufferWidth = newBins;
                visualizer->freqBins = newBins;
                visualizer->freq.recreateFreqTextureBuffer = TRUE;
            }
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_NORMALIZE:
        {
            GST_OBJECT_LOCK(visualizer);
            visualizer->freqNormalize = g_value_get_boolean(value);
            visualizer->freq.normalizer = 0u;
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_LOG_BINS:
        {
            GST_OBJECT_LOCK(visualizer);
            visualizer->logBins = g_value_get_boolean(value);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_NUM_SAMPLES:
        {
            GST_OBJECT_LOCK(visualizer);
            guint newNumSamples = next_power_of_two(g_value_get_uint(value));
            if (newNumSamples >= VISUALIZER_MIN_NUM_SAMPLES && newNumSamples <= VISUALIZER_MAX_NUM_SAMPLES)
            {
                visualizer->numSamples = newNumSamples;
            }
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_WINDOW:
        {
            GST_OBJECT_LOCK(visualizer);
            gst_visualizer_set_window(visualizer, g_value_get_enum(value));
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_HISTORY_COEFF:
        {
            GST_OBJECT_LOCK(visualizer);
            gfloat newHistoryCoeff = g_value_get_float(value);
            if (newHistoryCoeff >= VISUALIZER_MIN_HISTORY_COEFF && newHistoryCoeff <= VISUALIZER_MAX_HISTORY_COEFF)
            {
                visualizer->freq.historyCoeff = newHistoryCoeff;
            }
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        default:
        { // Unknown property.
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propId, pSpec);
            break;
        }
    }
}

static void gst_visualizer_get_property(GObject *object, guint propId, GValue *value, GParamSpec *pSpec)
{
    GST_DEBUG("GetProperty");

    GstVisualizer *visualizer = GST_VISUALIZER(object);

    switch (propId)
    {
        case PROP_SHADER_TYPE:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_enum(value, visualizer->shaderType);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_VERTEX_SRC:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_string(value, visualizer->vertexSrc->str);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FRAGMENT_SRC:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_string(value, visualizer->fragmentSrc->str);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_UNIFORMS:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_boxed(value, visualizer->uniforms);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FREQ_SAMPLING:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_enum(value, visualizer->freqSampling);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FREQ_EDGE:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_enum(value, visualizer->freqEdge);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_HISTORY:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_uint(value, visualizer->freqHistorySize);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_BINS:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_uint(value, visualizer->freqBins);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_NORMALIZE:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_boolean(value, visualizer->freqNormalize);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_LOG_BINS:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_boolean(value, visualizer->logBins);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_NUM_SAMPLES:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_uint(value, visualizer->numSamples);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_WINDOW:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_enum(value, visualizer->windowType);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        case PROP_FFT_HISTORY_COEFF:
        {
            GST_OBJECT_LOCK(visualizer);
            g_value_set_float(value, visualizer->freq.historyCoeff);
            GST_OBJECT_UNLOCK(visualizer);
            break;
        }
        default:
        { // Unknown property.
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propId, pSpec);
            break;
        }
    }
}

static void gst_visualizer_init(GstVisualizer *visualizer)
{
    GST_DEBUG("Init");
    GstGLFilter* filter = GST_GL_FILTER(visualizer);

    // TODO - Enable QOS?
    gst_base_transform_set_qos_enabled(GST_BASE_TRANSFORM(visualizer), TRUE);

    // Default values, changed before drawing anyways.
    filter->draw_attr_position_loc = 0u;
    filter->draw_attr_position_loc = 1u;

    visualizer->shaderType = VISUALIZER_DEFAULT_SHADER_TYPE;
    visualizer->vertexSrc = g_string_new(VISUALIZER_DEFAULT_VERT_SOURCE);
    visualizer->fragmentSrc = g_string_new(VISUALIZER_DEFAULT_FRAG_SOURCE);
    visualizer->uniforms = NULL;
    visualizer->freqSampling = VISUALIZER_DEFAULT_SAMPLING_TYPE;
    visualizer->freqEdge = VISUALIZER_DEFAULT_EDGE_TYPE;
    visualizer->freqHistorySize = VISUALIZER_DEFAULT_HISTORY;
    visualizer->freqBins = VISUALIZER_DEFAULT_BINS;
    visualizer->freqNormalize = VISUALIZER_DEFAULT_NORMALIZE;
    visualizer->logBins = VISUALIZER_DEFAULT_LOG_BINS;
    visualizer->numSamples = VISUALIZER_DEFAULT_NUM_SAMPLES;
    visualizer->windowType = VISUALIZER_DEFAULT_WINDOW_TYPE;

    visualizer->recreateShader = TRUE;
    visualizer->shader = NULL;
    visualizer->time = 0.0;
    gst_audio_info_init(&visualizer->audioInInfo);
    visualizer->bytesPerSample = 0;

    gst_visualizer_init_freq_texture(visualizer);
}

static void gst_visualizer_init_freq_texture(GstVisualizer *visualizer)
{
    visualizer->freq.sampleAcc = NULL;
    visualizer->freq.sampleAccW = NULL;
    visualizer->freq.sampleAccLength = 0u;
    visualizer->freq.sampleAccSize = 0u;
    visualizer->freq.fftOutput = NULL;
    visualizer->freq.fftInstance = NULL;
    visualizer->freq.windowType = VISUALIZER_DEFAULT_WINDOW_VALUE;
    visualizer->freq.fftOutputLength = 0u;
    visualizer->freq.normalizer = 0u;
    visualizer->freq.historyCoeff = VISUALIZER_DEFAULT_HISTORY_COEFF;
    visualizer->freq.freqTextureBuffer = NULL;
    visualizer->freq.recreateFreqTextureBuffer = FALSE;
    visualizer->freq.freqTextureBufferWidth = VISUALIZER_DEFAULT_BINS;
    visualizer->freq.freqTextureBufferHeight = VISUALIZER_DEFAULT_HISTORY;
    visualizer->freq.textureInternalFormat = GL_NONE;
    visualizer->freq.textureFormat = GL_NONE;
    visualizer->freq.textureType = GL_NONE;
    visualizer->freq.textureSampling = VISUALIZER_DEFAULT_SAMPLING_VALUE;
    visualizer->freq.textureEdgeSampling = VISUALIZER_DEFAULT_EDGE_VALUE;
    visualizer->freq.textureHandle = 0u;
    visualizer->freq.recreateTexture = FALSE;
    visualizer->freq.uploadTexture = FALSE;
}

static void gst_visualizer_dispose(GObject *object)
{
    GST_DEBUG("Dispose");
}

static void gst_visualizer_finalize(GObject *object)
{
    GST_DEBUG("Finalize");

    GstVisualizer *visualizer = GST_VISUALIZER(object);

    if (visualizer->freq.sampleAcc)
    {
        g_free(visualizer->freq.sampleAcc);
        g_free(visualizer->freq.sampleAccW);
        visualizer->freq.sampleAcc = NULL;
        visualizer->freq.sampleAccW = NULL;
    }

    if (visualizer->freq.fftOutput)
    {
        g_free(visualizer->freq.fftOutput);
        visualizer->freq.fftOutput = NULL;
    }

    if (visualizer->freq.fftInstance)
    {
        g_free(visualizer->freq.fftInstance);
        visualizer->freq.fftInstance = NULL;
    }

    if (visualizer->freq.freqTextureBuffer)
    {
        g_free(visualizer->freq.freqTextureBuffer);
        visualizer->freq.freqTextureBuffer = NULL;
    }

    if (visualizer->vertexSrc)
    {
        // TRUE for freeing the character data
        g_string_free(visualizer->vertexSrc, TRUE);
        visualizer->vertexSrc = NULL;
    }

    if (visualizer->fragmentSrc)
    {
        // TRUE for freeing the character data
        g_string_free(visualizer->fragmentSrc, TRUE);
        visualizer->fragmentSrc = NULL;
    }

    if (visualizer->uniforms)
    {
        gst_structure_free(visualizer->uniforms);
        visualizer->uniforms = NULL;
    }
}

static GstGLShader *gst_visualizer_recompile_shader(GstVisualizer *visualizer)
{
    GstGLContext *context = GST_GL_BASE_FILTER(visualizer)->context;
    GstGLShader *shader = NULL;
    GError *error = NULL;

    g_assert(context);
    g_assert(visualizer->vertexSrc);
    g_assert(visualizer->fragmentSrc);

    GST_OBJECT_LOCK(visualizer);
    shader = visualizer->shader;

    if (visualizer->recreateShader)
    {
        visualizer->recreateShader = FALSE;
        GstGLSLStage *stage = NULL;
        GstGLShader *newShader = gst_gl_shader_new(context);

        stage = gst_glsl_stage_new_with_string(context,
                                               GL_VERTEX_SHADER,
                                               GST_VISUALIZER_GLSL_VERSION,
                                               GST_VISUALIZER_GLSL_PROFILE,
                                               visualizer->vertexSrc->str);

        if (!stage)
        {
            if (error)
            {
                GST_ERROR_OBJECT(visualizer, "Failed to create vertex shader stage: \n%s", error->message);
            }
            else
            {
                GST_ERROR_OBJECT(visualizer, "Failed to create vertex shader stage");
            }
            gst_object_unref(newShader);
            GST_OBJECT_UNLOCK(visualizer);
            return shader;
        }

        if (!gst_gl_shader_compile_attach_stage(newShader, stage, &error))
        {
            if (error)
            {
                GST_ERROR_OBJECT(visualizer, "Unable to compile and attach vertex shader: \n%s", error->message);
            }
            else
            {
                GST_ERROR_OBJECT(visualizer, "Unable to compile and attach vertex shader");
            }
            gst_object_unref(stage);
            gst_object_unref(newShader);
            GST_OBJECT_UNLOCK(visualizer);
            return shader;
        }

        stage = gst_glsl_stage_new_with_string(context,
                                               GL_FRAGMENT_SHADER,
                                               GST_VISUALIZER_GLSL_VERSION,
                                               GST_VISUALIZER_GLSL_PROFILE,
                                               visualizer->fragmentSrc->str);

        if (!stage)
        {
            if (error)
            {
                GST_ERROR_OBJECT(visualizer, "Failed to create fragment shader stage: \n%s", error->message);
            }
            else
            {
                GST_ERROR_OBJECT(visualizer, "Failed to create fragment shader stage");
            }
            gst_object_unref(newShader);
            GST_OBJECT_UNLOCK(visualizer);
            return shader;
        }

        if (!gst_gl_shader_compile_attach_stage(newShader, stage, &error))
        {
            if (error)
            {
                GST_ERROR_OBJECT(visualizer, "Unable to compile and attach fragment shader: \n%s", error->message);
            }
            else
            {
                GST_ERROR_OBJECT(visualizer, "Unable to compile and attach fragment shader");
            }
            gst_object_unref(stage);
            gst_object_unref(newShader);
            GST_OBJECT_UNLOCK(visualizer);
            return shader;
        }

        if (!gst_gl_shader_link(newShader, &error))
        {
            if (error)
            {
                GST_ERROR_OBJECT(visualizer, "Unable to link shader program: \n%s", error->message);
            }
            else
            {
                GST_ERROR_OBJECT(visualizer, "Unable to link shader program");
            }
            gst_object_unref(newShader);
            GST_OBJECT_UNLOCK(visualizer);
            return shader;
        }

        if (visualizer->shader)
        {
            gst_object_unref(visualizer->shader);
        }
        visualizer->shader = newShader;
        shader = newShader;
        GST_DEBUG("%p", (void*)shader);
    }

    GST_OBJECT_UNLOCK(visualizer);

    return shader;
}

static gboolean gst_visualizer_set_custom_uniforms(GstVisualizer *visualizer)
{
    if (visualizer->uniforms && visualizer->shader)
    {
        GST_DEBUG("Setting custom uniforms");
        return gst_structure_foreach(visualizer->uniforms, gst_visualizer_set_uniform_callback, visualizer);
    }

    return TRUE;
}

static gboolean gst_visualizer_set_uniform_callback(GQuark fieldId, const GValue *value, gpointer object)
{
    GstVisualizer *visualizer = GST_VISUALIZER(object);

    const gchar *fieldName = g_quark_to_string(fieldId);

    switch (G_VALUE_TYPE(value))
    {
        case G_TYPE_INT:
        {
            gst_gl_shader_set_uniform_1i(visualizer->shader, fieldName, g_value_get_int(value));
            break;
        }
        case G_TYPE_FLOAT:
        {
            gst_gl_shader_set_uniform_1f(visualizer->shader, fieldName, g_value_get_float(value));
            break;
        }
        default:
        {
            GST_WARNING("Unknown value type passed for uniform %s", fieldName);
            break;
        }
    }

    return TRUE;
}

#ifndef VERSION
#define VERSION "0.0.1"
#endif
#ifndef PACKAGE
#define PACKAGE "Visualizer"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "Visualizer"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "TODO"
#endif

static gboolean plugin_init(GstPlugin * plugin)
{
    return gst_element_register(plugin, "visualizer", GST_RANK_NONE,
                                GST_TYPE_VISUALIZER);
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    visualizer,
    "Audio visualizer.",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

