/* GStreamer
 * Copyright (C) 2018 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_VISUALIZER_H_
#define _GST_VISUALIZER_H_

#define GST_USE_UNSTABLE_API
#include <gst/gl/gl.h>
#undef GST_USE_UNSTABLE_API
#include <gst/audio/audio.h>
#include <gst/fft/fft.h>

G_BEGIN_DECLS

#define GST_TYPE_VISUALIZER             (gst_visualizer_get_type())
#define GST_VISUALIZER(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_VISUALIZER, GstVisualizer))
#define GST_VISUALIZER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_VISUALIZER, GstVisualizerClass))
#define GST_IS_VISUALIZER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_VISUALIZER))
#define GST_IS_VISUALIZER_CLASS(obj)    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_VISUALIZER))
#define GST_VISUALIZER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_VISUALIZER, GstVisualizerClass))

typedef struct _GstVisualizer GstVisualizer;
typedef struct _GstVisualizerClass GstVisualizerClass;

/**
 * Shader presets.
 *
 * @VISUALIZER_SHADER_TYPE_NONE: Undefined value.
 * @VISUALIZER_SHADER_TYPE_CUSTOM: Use provided vertex and fragment shader source codes..
 */
typedef enum
{
    VISUALIZER_SHADER_TYPE_NONE = 0,
    VISUALIZER_SHADER_TYPE_CUSTOM,
    VISUALIZER_SHADER_TYPE_LAST
} GstVisualizerShaderType;

/**
 * Mag and Min filters set on the frequency texture.
 *
 * @VISUALIZER_SAMPLING_TYPE_NONE: Undefined value.
 * @VISUALIZER_SAMPLING_TYPE_NEAREST: Use the nearest value.
 * @VISUALIZER_SAMPLING_TYPE_LINEAR: Use linear interpolation of values.
 */
typedef enum
{
    VISUALIZER_SAMPLING_TYPE_NONE = 0,
    VISUALIZER_SAMPLING_TYPE_NEAREST,
    VISUALIZER_SAMPLING_TYPE_LINEAR,
    VISUALIZER_SAMPLING_TYPE_LAST
} GstVisualizerSamplingType;

/**
 * Edge sampling behavior.
 *
 * @VISUALIZER_EDGE_TYPE_NONE: Undefined value.
 * @VISUALIZER_EDGE_TYPE_REPEAT: Repeat the texture.
 * @VISUALIZER_EDGE_TYPE_MIRRORED_REPEAT: Repeat the texture but mirrored.
 * @VISUALIZER_EDGE_TYPE_CLAMP_TO_EDGE: Clamp texture coordinates to the edge.
 * @VISUALIZER_EDGE_TYPE_CLAMP_TO_BORDER: Clamp the texture coordinates to border and use border color.
 */
typedef enum
{
    VISUALIZER_EDGE_TYPE_NONE = 0,
    VISUALIZER_EDGE_TYPE_REPEAT,
    VISUALIZER_EDGE_TYPE_MIRRORED_REPEAT,
    VISUALIZER_EDGE_TYPE_CLAMP_TO_EDGE,
    VISUALIZER_EDGE_TYPE_CLAMP_TO_BORDER,
    VISUALIZER_EDGE_TYPE_LAST
} GstVisualizerEdgeType;

/**
 * Windowing function used before running FFT.
 *
 * @VISUALIZER_WINDOW_TYPE_NONE: Undefined value
 * @VISUALIZER_WINDOW_TYPE_RECTANGULAR: Rectangular window
 * @VISUALIZER_WINDOW_TYPE_HAMMING: Hamming window
 * @VISUALIZER_WINDOW_TYPE_HANN: Hanning window
 * @VISUALIZER_WINDOW_TYPE_BARTLETT: Bartlett window
 * @VISUALIZER_WINDOW_TYPE_BLACKMAN: Blackman window
 */
typedef enum
{
    VISUALIZER_WINDOW_TYPE_NONE = 0,
    VISUALIZER_WINDOW_TYPE_RECTANGULAR,
    VISUALIZER_WINDOW_TYPE_HAMMING,
    VISUALIZER_WINDOW_TYPE_HANN,
    VISUALIZER_WINDOW_TYPE_BARTLETT,
    VISUALIZER_WINDOW_TYPE_BLACKMAN,
    VISUALIZER_WINDOW_TYPE_LAST
} GstVisualizerWindowType;

#define VISUALIZER_MIN_HISTORY 2u
#define VISUALIZER_MAX_HISTORY 32u
#define VISUALIZER_DEFAULT_HISTORY VISUALIZER_MAX_HISTORY

#define VISUALIZER_MIN_BINS 0u
#define VISUALIZER_MAX_BINS 512u
#define VISUALIZER_DEFAULT_BINS VISUALIZER_MIN_BINS

#define VISUALIZER_MIN_NUM_SAMPLES 64u
#define VISUALIZER_MAX_NUM_SAMPLES 4096u
#define VISUALIZER_DEFAULT_NUM_SAMPLES 1024u

#define VISUALIZER_MIN_HISTORY_COEFF 0.0f
#define VISUALIZER_MAX_HISTORY_COEFF 1.0f
#define VISUALIZER_DEFAULT_HISTORY_COEFF 0.1f

#define VISUALIZER_DEFAULT_VERT_SOURCE "#version 330 core\n" \
                                       "layout(location = 0) in vec4 vertPos;\n" \
                                       "layout(location = 1) in vec2 vertUv;\n" \
                                       "out vec2 uv;" \
                                       "void main() {\n" \
                                       "  gl_Position = vertPos;\n" \
                                       "  uv = vertUv;\n" \
                                       "}"
#define VISUALIZER_DEFAULT_FRAG_SOURCE "#version 330 core\n" \
                                       "/// Frequency map with freqWidth x freqHeight values.\n" \
                                       "uniform sampler2D freqs;\n" \
                                       "/// Width of the viewport.\n" \
                                       "uniform float width;\n" \
                                       "/// Height of the viewport.\n" \
                                       "uniform float height;\n" \
                                       "/// Width of the frequency map.\n" \
                                       "uniform float freqWidth;\n" \
                                       "/// Height of the frequency map.\n" \
                                       "uniform float freqHeight;\n" \
                                       "/// Monotonic time.\n" \
                                       "uniform float time;\n" \
                                       "in vec2 uv;\n" \
                                       "out vec4 color;\n" \
                                       "void main() {\n" \
                                       "  float mag = texture(freqs, vec2(1.0 - uv.y, 1.0 - uv.x)).r;\n" \
                                       "  color = vec4(mag, 0.0, 0.0, 1.0);\n" \
                                       "}"
#define VISUALIZER_DEFAULT_NORMALIZE TRUE
#define VISUALIZER_DEFAULT_LOG_BINS TRUE

/**
 * @param vis Visualizer instance.
 * @param pointer to data in required format.
 * @param numSamples Number of samples in data array.
 */
typedef gboolean (*GstVisualizerAudioProcessFun)(GstVisualizer* vis, void* data, gsize numSamples);
/**
 * @param vis Visualizer instance.
 * @param numFreqs How many frequencies will be output.
 * @param numSamples How many samples will be used.
 */
typedef gboolean (*GstVisualizerFftDataCreateFun)(GstVisualizer* vis, gsize numFreqs, gsize numSamples);

struct FreqInfo
{
    // FFT info.

    /// Accumulation buffer for input signal.
    void *sampleAcc;
    /// Accumulation buffer for input signal, used for windowed frame.
    void *sampleAccW;
    /// Size of sample accumulator.
    gsize sampleAccLength;
    /// Number of accumulated samples in sample accumulator.
    gsize sampleAccSize;
    /// Output buffer for the FFT.
    void *fftOutput;
    /// Instance of the FFT.
    void *fftInstance;
    /// Window type used before applying FFT.
    GstFFTWindow windowType;
    /// Number of frequency samples in the FFT data array.
    gsize fftOutputLength;
    /// Normalization divisor for the current row of frequency buffer.
    GLfloat normalizer;
    /// What part of the history frequency should we keep when getting new data?
    GLfloat historyCoeff;

    // Frequency texture info.

    /// Buffer for the frequency texture.
    GLfloat *freqTextureBuffer;
    /// Flag specifying whether buffer should be re-created.
    gboolean recreateFreqTextureBuffer;
    /// Width of the texture - number of frequency bins.
    gsize freqTextureBufferWidth;
    /// Height of the texture - depth of history.
    gsize freqTextureBufferHeight;

    // OpenGL Texture info.

    /// Internal format of the GPU texture.
    GLint textureInternalFormat;
    /// Format of the GPU texture.
    GLenum textureFormat;
    /// Size type of the GPU texture.
    GLenum textureType;
    /// Sampling mode for the texture.
    GLenum textureSampling;
    /// Edge sampling mode for the texture.
    GLenum textureEdgeSampling;
    /// Texture used to store frequency info on the GPU.
    GLuint textureHandle;
    /// Flag specifying whether the texture should be recreated.
    gboolean recreateTexture;
    /// Flag specifying new data should be uploaded.
    gboolean uploadTexture;
};

struct _GstVisualizer
{
    GstGLFilter baseVisualizer;

    // Properties:

    /// Currently used shader type.
    GstVisualizerShaderType shaderType;
    /// Vertex shader source code.
    GString *vertexSrc;
    /// Fragment shader source code.
    GString *fragmentSrc;
    /// Uniform values sent to the shader by the user.
    GstStructure *uniforms;
    /// Sampling used for the frequency texture.
    GstVisualizerSamplingType freqSampling;
    /// Edge sampling used for the frequency texture.
    GstVisualizerEdgeType freqEdge;
    /// Size of history buffer for the frequency texture.
    guint freqHistorySize;
    /// Number of frequency bins used for the frequency texture.
    guint freqBins;
    /// Whether to normalize each line of frequency values.
    gboolean freqNormalize;
    /// Whether to divide frequencies into bins linearly, or logarithmically.
    gboolean logBins;
    /// Number of samples used in the FFT.
    guint numSamples;
    /// Windowing function used before applying FFT.
    GstVisualizerWindowType windowType;

    /// Flags for recreating the shader.
    gboolean recreateShader;
    /// Main shader used in visualization.
    GstGLShader *shader;
    /// Time passed to the shader.
    gdouble time;
    /// Information about audio coming through the sink pad.
    GstAudioInfo audioInInfo;

    /// Size of one audio sample in bytes.
    gsize bytesPerSample;
    /// Function used for processing audio.
    GstVisualizerAudioProcessFun audioProcessFun;
    /// Function used for allocating frequency buffer.
    GstVisualizerFftDataCreateFun fftDataCreateFun;
    /// Frequency data and GPU handle.
    struct FreqInfo freq;
};

struct _GstVisualizerClass
{
    GstGLFilterClass baseVisualizerClass;
};

GType gst_visualizer_get_type (void);

G_END_DECLS

#endif
