/* GStreamer
 * Copyright (C) 2018 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstvisualizer
 *
 * The visualizer element creates audio visualization.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v audiotestsrc ! visualizer ! glimagesink
 * ]|
 * This pipeline visualizes testing sound.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include "gstvisualizer.h"

GST_DEBUG_CATEGORY_STATIC (gst_visualizer_debug_category);
#define GST_CAT_DEFAULT gst_visualizer_debug_category

/* prototypes */

// GstGLBaseFilter
static gboolean gst_visualizer_gl_start(GstGLBaseFilter * base);
static void gst_visualizer_gl_stop(GstGLBaseFilter * base);

// GstGLFilter
static gboolean gst_visualizer_set_caps(GstGLFilter * trans,
    GstCaps * incaps, GstCaps * outcaps);
static gboolean gst_visualizer_filter(GstGLFilter * filter,
    GstBuffer * inbuf, GstBuffer * outbuf);
static gboolean gst_visualizer_filter_texture(GstGLFilter * filter,
    GstGLMemory * in_tex, GstGLMemory * out_tex);
static gboolean gst_visualizer_init_fbo(GstGLFilter *filter);
static gboolean gst_visualizer_hcallback(GstGLFilter * filter,
    GstGLMemory * in_tex, gpointer stuff);

// GObject
static void gst_visualizer_set_property(GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_visualizer_get_property(GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_visualizer_dispose(GObject * object);
static void gst_visualizer_finalize(GObject * object);

static gboolean plugin_init(GstPlugin *plugin);

#if 0
static GstCaps *gst_visualizer_transform_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps, GstCaps * filter);
static GstCaps *gst_visualizer_fixate_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps, GstCaps * othercaps);
static gboolean gst_visualizer_accept_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps);
static gboolean gst_visualizer_query (GstBaseTransform * trans,
    GstPadDirection direction, GstQuery * query);
static gboolean gst_visualizer_decide_allocation (GstBaseTransform * trans,
    GstQuery * query);
static gboolean gst_visualizer_filter_meta (GstBaseTransform * trans,
    GstQuery * query, GType api, const GstStructure * params);
static gboolean gst_visualizer_propose_allocation (GstBaseTransform * trans,
    GstQuery * decide_query, GstQuery * query);
static gboolean gst_visualizer_transform_size (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps, gsize size, GstCaps * othercaps,
    gsize * othersize);
static gboolean gst_visualizer_get_unit_size (GstBaseTransform * trans,
    GstCaps * caps, gsize * size);
static gboolean gst_visualizer_start (GstBaseTransform * trans);
static gboolean gst_visualizer_stop (GstBaseTransform * trans);
static gboolean gst_visualizer_sink_event (GstBaseTransform * trans,
    GstEvent * event);
static gboolean gst_visualizer_src_event (GstBaseTransform * trans,
    GstEvent * event);
static GstFlowReturn gst_visualizer_prepare_output_buffer (GstBaseTransform *
    trans, GstBuffer * input, GstBuffer ** outbuf);
static gboolean gst_visualizer_copy_metadata (GstBaseTransform * trans,
    GstBuffer * input, GstBuffer * outbuf);
static gboolean gst_visualizer_transform_meta (GstBaseTransform * trans,
    GstBuffer * outbuf, GstMeta * meta, GstBuffer * inbuf);
static void gst_visualizer_before_transform (GstBaseTransform * trans,
    GstBuffer * buffer);
static GstFlowReturn gst_visualizer_transform (GstBaseTransform * trans,
    GstBuffer * inbuf, GstBuffer * outbuf);
static GstFlowReturn gst_visualizer_transform_ip (GstBaseTransform * trans,
    GstBuffer * buf);
#endif

enum
{
  PROP_0
};

/* pad templates */

static GstStaticPadTemplate gst_visualizer_sink_template =
    GST_STATIC_PAD_TEMPLATE ("sink",
         GST_PAD_SINK,
         GST_PAD_ALWAYS,
         GST_STATIC_CAPS ("audio/x-raw, format = (string) " GST_AUDIO_NE(S16) ", "
             "rate = (int) [ 8000, 96000 ], "
             "channels = (int) 1, "
             "layout = (string) interleaved; "
             "audio/x-raw, "
             "format = (string) " GST_AUDIO_NE(S16) ", "
             "rate = (int) [ 8000, 96000 ], "
             "channels = (int) 2, "
             "channel-mask = (bitmask) 0x3, layout = (string) interleaved")
    );


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstVisualizer, gst_visualizer, GST_TYPE_GL_FILTER,
    GST_DEBUG_CATEGORY_INIT (gst_visualizer_debug_category, "visualizer", 0, "Audio visualizer"))

static void
gst_visualizer_class_init (GstVisualizerClass * klass)
{
    GObjectClass *gobjectClass = G_OBJECT_CLASS(klass);
    GstGLBaseFilterClass *glBaseFilterClass = GST_GL_BASE_FILTER_CLASS(klass);
    GstGLFilterClass *glFilterClass = GST_GL_FILTER_CLASS(klass);

    // Replace the sink pad with audio one.
    gst_element_class_add_static_pad_template (GST_ELEMENT_CLASS(klass), &gst_visualizer_sink_template);

    gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "Visualizer", "Generic", "Audio visualizer.",
      "Tomas Polasek<xpolas34@stud.fit.vutbr.cz>");

    gobjectClass->set_property = gst_visualizer_set_property;
    gobjectClass->get_property = gst_visualizer_get_property;
    gobjectClass->dispose = gst_visualizer_dispose;
    gobjectClass->finalize = gst_visualizer_finalize;

    glFilterClass->set_caps = gst_visualizer_set_caps;
    // TODO - Choose only one - filter or filter_texture and don't set the other one!
    glFilterClass->filter = gst_visualizer_filter;
    glFilterClass->filter_texture = gst_visualizer_filter_texture;
    glFilterClass->init_fbo = gst_visualizer_init_fbo;
    //glFilterClass->transform_internal_caps = ???;

    //glBaseFilterClass->gl_set_caps = gst_visualizer_set_caps;
    glBaseFilterClass->gl_start = gst_visualizer_gl_start;
    glBaseFilterClass->gl_stop = gst_visualizer_gl_stop;
    glBaseFilterClass->supported_gl_api = GST_GL_API_OPENGL3;
}

#if 0
    base_transform_class->transform_caps = GST_DEBUG_FUNCPTR (gst_visualizer_transform_caps);
    base_transform_class->fixate_caps = GST_DEBUG_FUNCPTR (gst_visualizer_fixate_caps);
    base_transform_class->accept_caps = GST_DEBUG_FUNCPTR (gst_visualizer_accept_caps);
    base_transform_class->set_caps = GST_DEBUG_FUNCPTR (gst_visualizer_set_caps);
    base_transform_class->query = GST_DEBUG_FUNCPTR (gst_visualizer_query);
    base_transform_class->decide_allocation = GST_DEBUG_FUNCPTR (gst_visualizer_decide_allocation);
    base_transform_class->filter_meta = GST_DEBUG_FUNCPTR (gst_visualizer_filter_meta);
    base_transform_class->propose_allocation = GST_DEBUG_FUNCPTR (gst_visualizer_propose_allocation);
    base_transform_class->transform_size = GST_DEBUG_FUNCPTR (gst_visualizer_transform_size);
    base_transform_class->get_unit_size = GST_DEBUG_FUNCPTR (gst_visualizer_get_unit_size);
    base_transform_class->start = GST_DEBUG_FUNCPTR (gst_visualizer_start);
    base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_visualizer_stop);
    base_transform_class->sink_event = GST_DEBUG_FUNCPTR (gst_visualizer_sink_event);
    base_transform_class->src_event = GST_DEBUG_FUNCPTR (gst_visualizer_src_event);
    base_transform_class->prepare_output_buffer = GST_DEBUG_FUNCPTR (gst_visualizer_prepare_output_buffer);
    base_transform_class->copy_metadata = GST_DEBUG_FUNCPTR (gst_visualizer_copy_metadata);
    base_transform_class->transform_meta = GST_DEBUG_FUNCPTR (gst_visualizer_transform_meta);
    base_transform_class->before_transform = GST_DEBUG_FUNCPTR (gst_visualizer_before_transform);
    base_transform_class->transform = GST_DEBUG_FUNCPTR (gst_visualizer_transform);
    base_transform_class->transform_ip = GST_DEBUG_FUNCPTR (gst_visualizer_transform_ip);

static void
gst_visualizer_init (GstVisualizer *visualizer)
{
}

void
gst_visualizer_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstVisualizer *visualizer = GST_VISUALIZER (object);

  GST_DEBUG_OBJECT (visualizer, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_visualizer_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstVisualizer *visualizer = GST_VISUALIZER (object);

  GST_DEBUG_OBJECT (visualizer, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_visualizer_dispose (GObject * object)
{
  GstVisualizer *visualizer = GST_VISUALIZER (object);

  GST_DEBUG_OBJECT (visualizer, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_visualizer_parent_class)->dispose (object);
}

void
gst_visualizer_finalize (GObject * object)
{
  GstVisualizer *visualizer = GST_VISUALIZER (object);

  GST_DEBUG_OBJECT (visualizer, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_visualizer_parent_class)->finalize (object);
}

static GstCaps *
gst_visualizer_transform_caps (GstBaseTransform * trans, GstPadDirection direction,
    GstCaps * caps, GstCaps * filter)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);
  GstCaps *othercaps;

  GST_DEBUG_OBJECT (visualizer, "transform_caps");

  othercaps = gst_caps_copy (caps);

  /* Copy other caps and modify as appropriate */
  /* This works for the simplest cases, where the transform modifies one
   * or more fields in the caps structure.  It does not work correctly
   * if passthrough caps are preferred. */
  if (direction == GST_PAD_SRC) {
    /* transform caps going upstream */
  } else {
    /* transform caps going downstream */
  }

  if (filter) {
    GstCaps *intersect;

    intersect = gst_caps_intersect (othercaps, filter);
    gst_caps_unref (othercaps);

    return intersect;
  } else {
    return othercaps;
  }
}

static GstCaps *
gst_visualizer_fixate_caps (GstBaseTransform * trans, GstPadDirection direction,
    GstCaps * caps, GstCaps * othercaps)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "fixate_caps");

  return NULL;
}

static gboolean
gst_visualizer_accept_caps (GstBaseTransform * trans, GstPadDirection direction,
    GstCaps * caps)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "accept_caps");

  return TRUE;
}

static gboolean
gst_visualizer_set_caps (GstBaseTransform * trans, GstCaps * incaps,
    GstCaps * outcaps)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "set_caps");

  return TRUE;
}

static gboolean
gst_visualizer_query (GstBaseTransform * trans, GstPadDirection direction,
    GstQuery * query)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "query");

  return TRUE;
}

/* decide allocation query for output buffers */
static gboolean
gst_visualizer_decide_allocation (GstBaseTransform * trans, GstQuery * query)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "decide_allocation");

  return TRUE;
}

static gboolean
gst_visualizer_filter_meta (GstBaseTransform * trans, GstQuery * query, GType api,
    const GstStructure * params)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "filter_meta");

  return TRUE;
}

/* propose allocation query parameters for input buffers */
static gboolean
gst_visualizer_propose_allocation (GstBaseTransform * trans,
    GstQuery * decide_query, GstQuery * query)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "propose_allocation");

  return TRUE;
}

/* transform size */
static gboolean
gst_visualizer_transform_size (GstBaseTransform * trans, GstPadDirection direction,
    GstCaps * caps, gsize size, GstCaps * othercaps, gsize * othersize)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "transform_size");

  return TRUE;
}

static gboolean
gst_visualizer_get_unit_size (GstBaseTransform * trans, GstCaps * caps,
    gsize * size)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "get_unit_size");

  return TRUE;
}

/* states */
static gboolean
gst_visualizer_start (GstBaseTransform * trans)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "start");

  return TRUE;
}

static gboolean
gst_visualizer_stop (GstBaseTransform * trans)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "stop");

  return TRUE;
}

/* sink and src pad event handlers */
static gboolean
gst_visualizer_sink_event (GstBaseTransform * trans, GstEvent * event)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "sink_event");

  return GST_BASE_TRANSFORM_CLASS (gst_visualizer_parent_class)->sink_event (
      trans, event);
}

static gboolean
gst_visualizer_src_event (GstBaseTransform * trans, GstEvent * event)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "src_event");

  return GST_BASE_TRANSFORM_CLASS (gst_visualizer_parent_class)->src_event (
      trans, event);
}

static GstFlowReturn
gst_visualizer_prepare_output_buffer (GstBaseTransform * trans, GstBuffer * input,
    GstBuffer ** outbuf)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "prepare_output_buffer");

  return GST_FLOW_OK;
}

/* metadata */
static gboolean
gst_visualizer_copy_metadata (GstBaseTransform * trans, GstBuffer * input,
    GstBuffer * outbuf)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "copy_metadata");

  return TRUE;
}

static gboolean
gst_visualizer_transform_meta (GstBaseTransform * trans, GstBuffer * outbuf,
    GstMeta * meta, GstBuffer * inbuf)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "transform_meta");

  return TRUE;
}

static void
gst_visualizer_before_transform (GstBaseTransform * trans, GstBuffer * buffer)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "before_transform");

}

/* transform */
static GstFlowReturn
gst_visualizer_transform (GstBaseTransform * trans, GstBuffer * inbuf,
    GstBuffer * outbuf)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "transform");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_visualizer_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  GstVisualizer *visualizer = GST_VISUALIZER (trans);

  GST_DEBUG_OBJECT (visualizer, "transform_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "visualizer", GST_RANK_NONE,
      GST_TYPE_VISUALIZER);
}
#endif

#ifndef VERSION
#define VERSION "0.0.1"
#endif
#ifndef PACKAGE
#define PACKAGE "Visualizer"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "Visualizer"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "TODO"
#endif

static gboolean plugin_init(GstPlugin * plugin)
{
    return gst_element_register (plugin, "visualizer", GST_RANK_NONE,
                                 GST_TYPE_VISUALIZER);
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    visualizer,
    "Audio visualizer.",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

