/**
 * @file Plane.cpp
 * @author Tomas Polasek
 * @brief Simple render-able plane for OpenGL.
 */

#include "Plane.h"

namespace Util
{
    const GLfloat Plane::VERTEX_BUFFER_DATA[] =
            {
                    //  x      y     z
                    -1.0f, -1.0f, 0.0f,
                    1.0f , -1.0f, 0.0f,
                    -1.0f, 1.0f , 0.0f,

                    -1.0f, 1.0f , 0.0f,
                    1.0f , -1.0f, 0.0f,
                    1.0f , 1.0f , 0.0f,
            };

    const GLfloat Plane::UV_BUFFER_DATA[] =
        {
            // u     v
            0.0f, 0.0f,
            1.0f, 0.0f,
            0.0f, 1.0f,

            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
        };

    Plane::Plane()
    { }

    void Plane::prepare()
    {
        // Create vertex array.
        glGenVertexArrays(1, &mVAId);
        if (!mVAId)
        {
            throw std::runtime_error("Unable to glGenVertexArrays!");
        }
        glBindVertexArray(mVAId);

        // Create vertex buffer object.
        glGenBuffers(1, &mVBId);
        if (!mVBId)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        glBindBuffer(GL_ARRAY_BUFFER, mVBId);
        // Set the vertex data.
        glBufferData(GL_ARRAY_BUFFER, sizeof(VERTEX_BUFFER_DATA), VERTEX_BUFFER_DATA, GL_STATIC_DRAW);

        // Create uv buffer object.
        glGenBuffers(1, &mUvBId);
        if (!mUvBId)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        glBindBuffer(GL_ARRAY_BUFFER, mUvBId);
        // Set the uv data.
        glBufferData(GL_ARRAY_BUFFER, sizeof(UV_BUFFER_DATA), UV_BUFFER_DATA, GL_STATIC_DRAW);

        // Unbind the buffer and array.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    void Plane::render()
    {
        // Bind the vertex array.
        glBindVertexArray(mVAId);

        // Location 0.
        glEnableVertexAttribArray(0);
        // Bind the vertex data.
        glBindBuffer(GL_ARRAY_BUFFER, mVBId);
        // Set attributes.
        glVertexAttribPointer(
                0,             // Layout location.
                3,             // 3 coordinates for each vertex.
                GL_FLOAT,      // Type of the vertex data.
                GL_FALSE,      // Not normalized.
                0,             // Stride, data are behind each other.
                nullptr        // No offset.
        );

        // Location 1.
        glEnableVertexAttribArray(1);
        // Bind the uv data.
        glBindBuffer(GL_ARRAY_BUFFER, mUvBId);
        // Set attributes.
        glVertexAttribPointer(
            1,             // Layout location.
            2,             // 2 coordinates for each uv.
            GL_FLOAT,      // Type of the vertex data.
            GL_FALSE,      // Not normalized.
            0,             // Stride, data are behind each other.
            nullptr        // No offset.
        );

        // Draw the triangles.
        glDrawArrays(GL_TRIANGLES, 0, NUM_VERTICES);

        // Disable the attribute.
        glDisableVertexAttribArray(0);
        // Disable the vertex array.
        glBindVertexArray(mVAId);
    }

    Plane::~Plane()
    { destroy(); }

    void Plane::destroy()
    {
        if (mVAId)
        {
            glDeleteVertexArrays(1, &mVAId);
            mVAId = 0;
        }

        if (mVBId)
        {
            glDeleteBuffers(1, &mVBId);
            mVBId = 0;
        }

        if (mUvBId)
        {
            glDeleteBuffers(1, &mUvBId);
            mUvBId = 0;
        }
    }
}
