/**
 * @file Util.cpp
 * @author Tomas Polasek
 * @brief Utility functions and classes.
 */

#include "Util.h"

namespace Util
{
    void readFileIntoString(const std::string &filename, std::string &output)
    {
        std::ifstream file(filename);
        if (!file.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open file \"") + filename + "\"");
        }

        // Clear any existing data in output string.
        output.clear();

        // Reserve the string size upfront.
        file.seekg(0, std::ios::end);
        output.reserve(file.tellg());
        file.seekg(0, std::ios::beg);

        // Read the whole file into the string.
        output.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    }
}
