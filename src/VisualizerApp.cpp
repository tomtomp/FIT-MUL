/**
 * @file VisualizerApp.cpp
 * @author Tomas Polasek
 * @brief Main application class for the Visualizer app.
 */

#include "VisualizerApp.h"

VisualizerApp::VisualizerApp(int argc, char *argv[])
{
    // Parse user provided arguments.
    parseArgs(argc, argv);

    // Initialize libraries
    initSDL();
    mWindow.init(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE);
    initGLEW();
    initGst(argc, argv);

    // Select source
    std::string sourceType;
    if (mUseMicrophone)
    { // Use microphone source element.
        sourceType = Gst::Pipeline::MICROPHONE_SOURCE;
    }
    else
    { // Use uri source element.
        sourceType = Gst::Pipeline::URI_SOURCE;
    }

    // Build pipeline.
    if (mMute)
    { // Mute pipeline.
        mPipeline.initMute(mWindow.getDisplay(), mWindow.createWorkerContext(), argv[0], sourceType);
    }
    else
    { // Reproduction pipeline.
        mPipeline.init(mWindow.getDisplay(), mWindow.createWorkerContext(), argv[0], sourceType);
    }
}

VisualizerApp::~VisualizerApp()
{
    SDL_Quit();
}

int VisualizerApp::run()
{
    bool running{true};
    bool playing{true};

    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_ESCAPE, [&running] () {
        running = false;
    });

    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_SPACE, [this, &playing] () {
        if (playing)
        {
            mPipeline.pause();
            playing = false;
        }
        else
        {
            mPipeline.play();
            playing = true;
        }
    });

    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_r, [this] () {
        reloadUserShaders();
        useUserShaders();
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_s, [this] () {
        mPipeline.nextFreqSampling();
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_e, [this] () {
        mPipeline.nextFreqEdge();
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_u, [this] () {
        mPipeline.changeFftHistory(VISUALIZER_HISTORY_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_j, [this] () {
        mPipeline.changeFftHistory(1.0f / VISUALIZER_HISTORY_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_i, [this] () {
        mPipeline.changeFftBins(VISUALIZER_BINS_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_k, [this] () {
        mPipeline.changeFftBins(1.0f / VISUALIZER_BINS_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_m, [this] () {
        mPipeline.setAutoFftBins();
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_o, [this] () {
        mPipeline.changeFftNumSamples(VISUALIZER_SAMPLES_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_l, [this] () {
        mPipeline.changeFftNumSamples(1.0f / VISUALIZER_SAMPLES_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_y, [this] () {
        mPipeline.changeFftHistoryCoeff(VISUALIZER_HISTORY_COEFF_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_h, [this] () {
        mPipeline.changeFftHistoryCoeff(-VISUALIZER_HISTORY_COEFF_CHANGE);
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_n, [this] () {
        mPipeline.flipFftNormalize();
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_b, [this] () {
        mPipeline.flipFftLogBins();
    });
    mWindow.setAction(SDL_KEYDOWN, KMOD_NONE, SDLK_w, [this] () {
        mPipeline.nextFftWindow();
    });
    mWindow.setWindowResizedCb([this] (GLsizei width, GLsizei height) {
        mPipeline.pause();
        mPipeline.setupFrameInfo(width, height);
        mPipeline.play();
    });

    int width{0};
    int height{0};
    mWindow.getDimensions(width, height);
    mPipeline.setupFrameInfo(width, height);

    // Use any user provided shaders.
    useUserShaders();

    if (!mUseMicrophone)
    { // We need to set which URI input should be used.
        mPipeline.setupUri(mAudioInput);
    }
    else
    { // Else finalize the pipeline for microphone input.
        mPipeline.setupMicrophone();
    }

    // Start playing.
    mPipeline.play();

    while (running)
    {
        mPipeline.pollEvents();

        if (mWindow.pollEvents())
        { // Application quitting.
            running = false;
        }

        mWindow.draw(mPipeline.mapFrameTexture());
    }

    // Hide window to signalize the application is quitting...
    mWindow.hide();

    std::cout << "Attempting to gracefully quit GStreamer pipeline." << std::endl;
    mPipeline.quit();
    std::cout << "Exited GStreamer pipeline successfully." << std::endl;

    return EXIT_SUCCESS;
}

void VisualizerApp::initSDL()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        throw std::runtime_error(std::string("Unable to initialize SDL! : ") + SDL_GetError());
    }

    int result{0};

    // Use core profile.
    result += SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // OpenGL version 3.3 .
    result += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    result += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);

    // Double-buffered output.
    result += SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // Contexts should share data.
    result += SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);

    if (result != 0)
    {
        throw std::runtime_error(std::string("Unable to set OpenGL attributes for SDL! : ") + SDL_GetError());
    }
}

void VisualizerApp::initGLEW()
{
    /*
    glewExperimental = GL_TRUE;
    auto res = glewInit();
    if (res != GLEW_OK)
    {
        static_assert(sizeof(char) == sizeof(GLubyte), "Just to be sure...");
        throw std::runtime_error(std::string("Unable to initialize GLEW! : ") + reinterpret_cast<const char*>(glewGetErrorString(res)));
    }
     */
}

void VisualizerApp::initGst(int argc, char *argv[])
{
    gst_init(&argc, &argv);
}

void VisualizerApp::parseArgs(int argc, char *argv[])
{
    // Reset microphone flag.
    mUseMicrophone = false;

    // Reset muteness flag.
    mMute = false;

    // Disable getopt error messages.
    opterr = 0;

    // Option letter.
    int opt{0};

    while ((opt = getopt(argc, argv, GETOPT_FORMAT)) != -1)
    { // For all available parameters.
        switch (opt)
        {
            case 'f':
            { // Audio file input.
                if (!mAudioInput.empty() || mUseMicrophone)
                {
                    printHelp();
                    throw std::runtime_error("Precisely one of -f or -m MUST be specified!");
                }
                mAudioInput = optarg;
                break;
            }
            case 'm':
            { // Microphone audio input.
                if (!mAudioInput.empty() || mUseMicrophone)
                {
                    printHelp();
                    throw std::runtime_error("Precisely one of -f or -m MUST be specified!");
                }
                mUseMicrophone = true;
                break;
            }
            case 'V':
            { // Vertex shader source code filename.
                mVertSrcFilename = optarg;
                break;
            }
            case 'F':
            { // Fragment shader source code filename.
                mFragSrcFilename = optarg;
                break;
            }
            case 'h':
            { // Print help message.
                printHelp();
                break;
            }
            case 'q':
            { // Quiet.
                mMute = true;
                break;
            }
            case ':':
            { // Missing option value.
                printHelp();
                throw std::runtime_error(std::string("Missing value for option '") + static_cast<char>(optopt) + "'!");
            }
            case '?':
            { // Unknown option.
                printHelp();
                throw std::runtime_error(std::string("Unknown option specified '") + static_cast<char>(optopt) + "'!");
            }
            default:
            { // Getopt returned unknown value.
                throw std::runtime_error(std::string("Getopt returned unknown value '") + static_cast<char>(opt) + "'!");
            }
        }
    }

    if (mAudioInput.empty() && !mUseMicrophone)
    {
        printHelp();
        throw std::runtime_error("Precisely one of -f or -m MUST be specified!");
    }

    if (!mAudioInput.empty())
    {
        convertInputToUri(mAudioInput);
    }

    reloadUserShaders();
}

void VisualizerApp::printHelp()
{
    std::cout << HELP_MESSAGE << std::endl;
}

void VisualizerApp::convertInputToUri(std::string &audioInput)
{
    // Simple test, if the string contains "://" then it is probably URI.
    if (audioInput.find("://") != audioInput.npos)
    { // We probably got URI, conversion complete.
        return;
    }
    // Else user specified a relative filename.

    // Calculate absolute path to the file.
    char *absFilePath = realpath(audioInput.c_str(), nullptr);

    if (!absFilePath)
    {
        throw std::runtime_error("Unable to calculate absolute path to file \"" + audioInput +"\"");
    }

    // Construct URI with "FILE://" prefix.
    audioInput = std::string("FILE://") + absFilePath;

    // Free resources allocated by realpath function.
    free(absFilePath);
}

void VisualizerApp::reloadUserShaders()
{
    if (!mVertSrcFilename.empty())
    {
        Util::readFileIntoString(mVertSrcFilename, mVertSrc);
    }

    if (!mFragSrcFilename.empty())
    {
        Util::readFileIntoString(mFragSrcFilename, mFragSrc);
    }
}

void VisualizerApp::useUserShaders()
{
    mPipeline.setUserShaders(mVertSrc, mFragSrc);
}

