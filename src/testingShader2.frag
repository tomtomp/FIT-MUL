#version 330 core

/// Number PI.
#define M_PI 3.141592653589793238462643383
/// Doulbe number PI.
#define M_2PI 2.0 * 3.141592653589793238462643383
/// Golden ratio.
#define M_PHI 1.618033988749894848204586834

uniform sampler2D freqs;
uniform float width;
uniform float height;
uniform float freqWidth;
uniform float freqHeight;
uniform float time;

in vec2 uv;
out vec4 color;

void main() {
    /*
    float yCoord = (pow(2.0, 8.0 * (1.0 - uv.y)) - 1.0) / 255.0;
    float xCoord = 1.0 - uv.x;
    float mag = texture(freqs, vec2(xCoord, yCoord)).r;
    color = vec4(abs(sin(time) * log(mag + 1.0)), abs(cos(time) * log2(mag + 1.0)), abs(sin(time + M_PI/3.0) * log(mag + 1.0) / log(10)), 1.0);
    */
    /*
    const vec2 middle = vec2(0.5, 0.5);
    vec2 mUv = uv - middle;
    float normalLength = length(middle);

    float r = length(mUv) / normalLength;
    float phi = (atan(mUv.y, mUv.x) / (M_2PI)) + 0.5;

    float xCoord = (pow(2.0, 8.0 * phi) - 1.0) / 255.0;
    float yCoord = r;

    float mag = texture(freqs, vec2(xCoord, yCoord)).r;
    color = vec4(abs(sin(time) * log(mag + 1.0)), abs(cos(time) * log2(mag + 1.0)), abs(sin(time + M_PI/3.0) * log(mag + 1.0) / log(10)), 1.0);
    */

    /*
    float smoothArea = 0.05;
    float yCoord = 1.0 - uv.y;
    float xCoord = (pow(2.0, 8.0 * uv.x) - 1.0) / 255.0;
    float mag = log2(1.0 + texture(freqs, vec2(xCoord, 0.0)).r);
    smoothArea = abs(texture(freqs, vec2(xCoord, 0.01)).r - mag);
    mag = 10 * log(pow(2.0, 0.5 * mag)) / log(10.0);
    if (yCoord < mag + smoothArea)
    {
        float pst = 1.0 - smoothstep(0.0, 0.05, yCoord - mag);
        float rx = (0.2 - uv.x) * 2.0;
        float r = pst * exp(-M_PI * rx * rx);
        float gx = (0.5 - uv.x) * 2.0;
        float g = pst * exp(-M_PI * gx * gx);
        float bx = (0.8 - uv.x) * 2.0;
        float b = pst * exp(-M_PI * bx * bx);

        color = vec4(r, g, b, 1.0);
    }
    else
    {
        color = vec4(0.0, 0.0, 0.0, 1.0);
    }
    */

    // Aspect ratio.
    vec2 ASPECT = vec2(width / height, 1.0);

    // UV with corrected aspect ratio.
    vec2 aUv = uv * ASPECT;

    // Middle of the circle.
    const vec2 MIDDLE = vec2(0.5, 0.5);
    // Radius of the circle
    const float RADIUS = 0.2;
    // Thickness of the inner ring.
    const float INNER_RING = 0.01;
    // Which frequency from the map is used as bass line.
    const float BASS_FREQ = 0.01;
    // TransformUV to polar coordinates, middle at MIDDLE.
    vec2 fromMiddle = aUv - ASPECT * MIDDLE;
    float fromMiddleL = length(fromMiddle);
    // Radius of given point.
    float r = fromMiddleL;
    // Calculate angle from middle.
    float phi = atan(fromMiddle.x, -fromMiddle.y);

    float yCoord = 1.0 - r;
    //float xCoord = (pow(2.0, 8.0 * phi) - 1.0) / 255.0;
    //float xCoord = floor(phi / width) * width;
    // Phase of given point, transformed from <-PI, PI> to <1.0, 0.0, 1.0>.
    float xCoord = abs(phi / M_PI);
    float mag = log2(2.0 + texture(freqs, vec2(xCoord, 0.0)).r) - 1.0;
    //float mag = texture(freqs, vec2(xCoord, 0.0)).r;
    float smoothArea = abs(texture(freqs, vec2(xCoord, 0.01)).r - mag);
    mag = 10 * log(pow(2.0, 0.5 * mag)) / log(10.0) * 0.7;

    float outerTarget = abs(0.3 * (mag + smoothArea) + RADIUS);
    //float innerTarget = 0.5 * (mag + smoothArea) + RADIUS - 0.01;
    float innerTarget = RADIUS - INNER_RING;

    float rx = (0.2 - xCoord) * 2.0;
    float rc = exp(-M_PI * rx * rx);
    float gx = (0.5 - xCoord) * 2.0;
    float gc = exp(-M_PI * gx * gx);
    float bx = (0.8 - xCoord) * 2.0;
    float bc = exp(-M_PI * bx * bx);

    //if (r < outerTarget && r > innerTarget)
    if (r > innerTarget)
    {
        float pst = smoothstep(0.0, 0.05 * outerTarget, outerTarget - r);
        //float pst = 1.0 - smoothstep(0.0, 0.05, outerTarget - r);
        float pstInner = 1.0 - smoothstep(0.0, INNER_RING, innerTarget - r);

        color = vec4(pst * pstInner * vec3(rc, gc, bc), 1.0);

        //float pstSide1 = smoothstep(0.1, 0.3, fract(xCoord * 64));
        //float pstSide2 = smoothstep(0.1, 0.3, fract((1.0 - xCoord) * 64));
        //color = vec4(pst * pstInner * pstSide1 * pstSide2 * vec3(rc, gc, bc), 1.0);
    }
    else
    {
        const int MAX_ITER = 50;
        const vec2 SCALE = vec2(8.0, 8.0);
        const float TIME_FACTOR = 3.0;
        vec2 volumeScale = vec2(texture(freqs, vec2(BASS_FREQ, 0.0)).r * 2.0);
        vec2 fractalPos = ASPECT * (SCALE + volumeScale) * vec2(fromMiddle.x, fromMiddle.y);

        /*
        vec2 z = vec2(0.0);
        vec2 c = fractalPos;
        */
        vec2 z = fractalPos;
        //vec2 c = vec2(-0.4, 0.6);
        vec2 c = vec2(0.7885 * cos(time / TIME_FACTOR), -0.7885 * sin(time / TIME_FACTOR));
        //vec2 c = vec2(-0.4 * cos(time), 0.6 * sin(time));
        int iter;

        for (iter = 0; iter < MAX_ITER; ++iter)
        {
            // z_n+1 = z_n ^ 2 + c
            z = vec2(z.x * z.x - z.y * z.y + c.x, 2.0 * z.x * z.y + c.y);

            if (length(z) > 4.0)
            {
                break;
            }
        }

        float smoothIter = iter + 1.0 - log(log(length(z))) / log(2.0);
        float smoothColor = float(smoothIter) / MAX_ITER;

        float pstInner = 1.0 - smoothstep(0.0, INNER_RING, innerTarget - r);

        /*
        if (iter == MAX_ITER)
        {
            smoothColor = 1.0;
        }
        */

        //color = vec4((1.0 - smoothColor) * vec3(rc, gc, bc), 1.0);
        color = vec4(mix((1.0 - smoothColor) * vec3(1.0), vec3(rc, gc, bc), pstInner), 1.0);
    }

    /*
    float volumeRing = smoothstep(0.2, 0.7, (texture(freqs, vec2(BASS_FREQ, 0.0)).r / 2.0)) + 0.2 + sin(time + 5.0 * phi) * 0.05;
    float ring1 = smoothstep(0.0, 0.1, r - volumeRing);
    float ring2 = smoothstep(0.0, 0.1, volumeRing - r + 0.1);

    color = vec4(ring1 * ring2 * vec3(1.0), 1.0);
    */
}

